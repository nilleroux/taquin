/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.extra;

import java.util.HashMap;

import org.javatuples.Pair;

/**
 * The Class FastMapInitializer allows to generate Maps on the fly.
 */
public final class FastMapInitializer {
	
	/**
	 * Returns a HashMap initialized with entries.
	 * This function is actually SafeVarargs since no cast is done. Generic types will always refer to
	 * a type that is actually the object's type they points to. This avoids ClassCastException risks.
	 * Obviously, you shouldn't pass different types as variadic argument.
	 * http://docs.oracle.com/javase/7/docs/api/java/lang/SafeVarargs.html
	 * 
	 *
	 * @param <K> Entry type.
	 * @param <V> Value type.
	 * @param entries All the entries in the HashMap.
	 * @return A new HashMap initialized with entries.
	 */
	@SafeVarargs
    public static <K, V> HashMap<K, V> getHashMap(final Pair<K, V>... entries) {
		final HashMap<K, V> map = new HashMap<>();
		for (final Pair<K, V> entry : entries)
			map.put(entry.getValue0(), entry.getValue1());
		return map;
	}
}
