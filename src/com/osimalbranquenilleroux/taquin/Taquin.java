/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.osimalbranquenilleroux.taquin.controller.ControllerHandler;
import com.osimalbranquenilleroux.taquin.controller.MainController;
import com.osimalbranquenilleroux.taquin.model.Model;

/**
 * The Taquin Game main class.
 */
public class Taquin {

	/**
	 * Starts a new game.
	 */
	public Taquin() {
        try {
            // Change look and feel to system's
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException
                |ClassNotFoundException
                |InstantiationException
                |IllegalAccessException e) { // If it fails, just use default L&F
        }
        
		Model taquinModel = new Model();
		ControllerHandler controllerHandler = new ControllerHandler();
		controllerHandler.launchController(taquinModel, "main_controller", MainController.class).act();
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new Taquin();
	}

}
