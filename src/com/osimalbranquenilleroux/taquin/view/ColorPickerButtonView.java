/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;

import com.osimalbranquenilleroux.taquin.controller.ParametersController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * Class ColorPickerButtonView.
 * Button that allows to pick a color.
 */
class ColorPickerButtonView extends AbstractView<ParametersController>
{
	
	/** The button. */
	private final JButton _button = new JButton(_model.getStringProvider().getString("choose_a_color"));
	
	/**
	 * Instantiates a new color picker button view.
	 *
	 * @param model the model (useful to set text on the button in the right language)
	 * @param parentFrame the parent frame (will be frozen when picking the button)
	 * @param defaultColor the default color of the button
	 */
	public ColorPickerButtonView(final ImmutableModel model, final JFrame parentFrame, final Color defaultColor)
	{
		super(model, null);
		
		_button.setBackground(defaultColor);
        _button.setContentAreaFilled(false);
        _button.setOpaque(true);
		_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color =
                		JColorChooser.showDialog(parentFrame, _model.getStringProvider().getString("choose_a_color"), Color.blue);
                if (color != null)
                	_button.setBackground(color);
            }
        });
	}
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _button;
	 }
	 
	 /**
 	 * Gets the picked color.
 	 *
 	 * @return the picked color
 	 */
 	Color getPickedColor() {
		 return _button.getBackground();
	 }
}
