/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.osimalbranquenilleroux.taquin.controller.MainController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * The Class MainViewWonDialogView.
 * The view that shows "you have won" message.
 */
final class MainViewWonDialogView extends AbstractView<MainController> {
	
	/**
	 * Interface Command.
	 * Simple interface that runs a command. Replacement trick for C's function pointers.
	 */
	interface Command {
	    
    	/**
    	 * Run any command.
    	 */
    	void runCommand();
	}
	
	/** Parent frame (that should be frozen). */
	private final JFrame _parentFrame;
	
	/**
	 * Instantiates a new main view won dialog view.
	 *
	 * @param model the model
	 * @param controller the controller
	 * @param parentFrame the parent frame
	 */
	public MainViewWonDialogView(final ImmutableModel model, final MainController controller, final JFrame parentFrame) {
		super(model, controller);
		_parentFrame = parentFrame;
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
	 */
	@Override
	Component getComponent() {
		return null;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#display()
	 */
	@Override
	public void display() {
		final Object[] options = { _model.getStringProvider().getString("retry"), _model.getStringProvider().getString("quit") };
		final Command[] optionsActions =  {
				new Command() {
							@Override
							public void runCommand() { _controller.fireNewGameEvent(); }
						},
				new Command() {
							@Override
							public void runCommand() { _controller.fireCloseEvent(); }
						}
		};
		int result = JOptionPane.showOptionDialog(
				_parentFrame,
				String.format(_model.getStringProvider().getString("you_have_won"),
				        _model.getPlayer().getMoves())
				    + "\n" + _model.getStringProvider().getString("what_do_you_want_to_do"),
			    _model.getStringProvider().getString("congratulations"),
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[0]);
		optionsActions[result].runCommand();
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
	 */
	@Override
	public void close() {
		//Nothing to do
	}
}
