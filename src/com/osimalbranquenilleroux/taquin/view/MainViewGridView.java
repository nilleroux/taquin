/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import javax.swing.JComponent;

import com.osimalbranquenilleroux.taquin.controller.MainController;
import com.osimalbranquenilleroux.taquin.model.Grid;
import com.osimalbranquenilleroux.taquin.model.Grid.Direction;
import com.osimalbranquenilleroux.taquin.model.ImmutableGridToken;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;
import com.osimalbranquenilleroux.taquin.model.ModelUpdateEvent;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;

import org.javatuples.Pair;
import org.javatuples.Triplet;

/**
 * Class MainViewGridView.
 * Handles the grid component.
 */
final class MainViewGridView extends AbstractView<MainController> {
	
	/** The duration of a token interpolation. */
	private final static int _TOKEN_INTERPOLATION_DURATION = 1000; // in ms
	
	/** The duration of a token opacity animation. */
	private final static int _TOKEN_OPACITY_ANIMATION_DURATION = 1000;
	
	/** The grid. */
	private final JGridComponent _jGrid = new JGridComponent();
	
	/** Currently running interpolations.
	 * Pair<Integer:row of the animated token, Integer: column of the animated token>
	 *  => Triplet<Grid.Direction: the Direction of the token,
	 *             Long: the start time of the animation,
	 *             Long: the theoretical end time of the animation>
	 */
	private final ConcurrentHashMap<Pair<Integer, Integer>, Triplet<Grid.Direction, Long, Long>> _tokenInterpolations =
			new ConcurrentHashMap<>(); // (row,col)->(Direction, Animation start time, Animation end time)
	
	/** Currently running opacity animations.
     * ImmutableGridToken: the token that undergoes the animation
     *  => Pair<Long: the start time of the animation,
     *          Long: the theoretical end time of the animation>
     */
	private final ConcurrentHashMap<ImmutableGridToken, Pair<Long, Long>> _tokenOpacityAnimation = new ConcurrentHashMap<>();
	
	/** Boolean that is set to true when a thread generating frame is running.
	 * Numerous threads should not generate frames at the same time.
	 */
	private boolean _frameGeneratorRunning = false;
	
	/**
	 * Class JGridComponent. Representation of the Grid for the GridView.
	 */
	private class JGridComponent extends JComponent {
		
		/** serialVersionUID. */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Instantiates a new grid component.
		 */
		public JGridComponent() {
			addMouseListener(_mouseAdapter);
		}
		
		/** EventListener that catches the mouse clicks and sends them to the controller. */
		private final MouseAdapter _mouseAdapter = new MouseAdapter() {
			private Pair<Integer, Integer> _clickCell = null;
			
			@Override
			public void mousePressed(MouseEvent e) {
				_clickCell = clickPositionToCell(e.getX(), e.getY());
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
			    // Check if the click is valid
			    // (i.e. the mouse-press was performed on the same cell than the mouse-release)
				if (_clickCell != null && clickPositionToCell(e.getX(), e.getY()).equals(_clickCell))
					_controller.fireCellClickEvent(_clickCell.getValue0(), _clickCell.getValue1());
				_clickCell = null;
			}
			
			/**
			 * Converts click coordinates to coordinates in the grid.
			 * @param x the X coordinate of the click
			 * @param y the Y coordinate of the click
			 * @return the position of the click Pair<Integer: row, Integer: column>
			 */
			private Pair<Integer, Integer> clickPositionToCell(final int x, final int y) {
				final Pair<Integer, Integer> offset = getOffset();
				return new Pair<>((int)Math.floor((float)(y - offset.getValue1()) / cellSideLength()),
						(int)Math.floor((float)(x - offset.getValue0()) / cellSideLength()));
			}
		};
		
		/**
		 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
		 * Paints each token.
		 */
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2 = (Graphics2D)g.create();
			
			RenderingHints renderingHints = new RenderingHints(
			        RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g2.setRenderingHints(renderingHints);
			
			final int cellSideLength = cellSideLength();
			final Pair<Integer, Integer> offset = getOffset();
			
			g2.setFont(new Font("Verdana", Font.BOLD, cellSideLength / 2));
			
			// paint each non-moving components.
			final int gridRows = _model.getGrid().getGridParameters().getSize().getValue0().intValue();
			final int gridColumns = _model.getGrid().getGridParameters().getSize().getValue1().intValue();
			for (int i = 0; i < gridRows; ++i)
				for (int j = 0; j < gridColumns; ++j)
					if (!isBeingAnimated(i, j))
						drawToken(g2, i, j, cellSideLength, offset);
			
			// paint components that are currently moving
			drawMovingTokens(g2);
		}
		
		/**
		 * Draw all tokens that are currently undergoing an interpolation.
		 *
		 * @param g the graphics on which we should draw
		 */
		private void drawMovingTokens(Graphics2D g) {
			final int cellSideLength = cellSideLength();
			final Pair<Integer, Integer> offset = getOffset();
			final long currentTime = System.currentTimeMillis();
			
			for (Entry<Pair<Integer, Integer>, Triplet<Direction, Long, Long>> tokenInterpolation
					: _tokenInterpolations.entrySet()) {
				
				final long animStartTime = tokenInterpolation.getValue().getValue1();
				final long animEndTime = tokenInterpolation.getValue().getValue2();
				final float animationProgressionRatio = (currentTime - animStartTime)	/ (float)(animEndTime - animStartTime);
				final int tokenDistFromCurrentPosition = (int)(cellSideLength * (1.f - animationProgressionRatio));
				final Pair<Integer, Integer> signs = new Pair<>(
						tokenInterpolation.getValue().getValue0().opposite().delta(1).getValue1(), // X sign (-1, 0 or 1)
						tokenInterpolation.getValue().getValue0().opposite().delta(1).getValue0()); // Y sign (-1, 0 or 1)
				
				// Draw token with the modified offset
				drawToken(g,
						tokenInterpolation.getKey().getValue0(), tokenInterpolation.getKey().getValue1(),
						cellSideLength,
						new Pair<>(
								offset.getValue0() + signs.getValue0() * tokenDistFromCurrentPosition,
								offset.getValue1() + signs.getValue1() * tokenDistFromCurrentPosition
						)
				);
			}
		}

		/**
		 * Checks whether the given cell is currently moving.
		 *
		 * @param row the row
		 * @param column the column
		 * @return true, if the given cell is currently moving
		 */
		private boolean isBeingAnimated(final int row, final int column) {
			if (_tokenInterpolations.containsKey(new Pair<Integer, Integer>(row, column)))
				return true;
			return false;
		}

		/**
		 * Draws the given token.
		 *
		 * @param g2 the graphic element on which we should draw
		 * @param row the row of the token
		 * @param column the column of the token
		 * @param cellSideLength Side length of a cell
		 * @param offset the offset of the grid on its pane.
		 */
		private void drawToken(Graphics2D g2, final int row, final int column,
				final int cellSideLength, final Pair<Integer, Integer> offset) {
			
		    // Don't draw the token if it is hidden
			if (_model.getGrid().getToken(row, column).isHidden())
				return;
	
			// Get its colors
			Color tokenColor = 	_model.getGrid().getColorOfPosition(
					_model.getGrid().getToken(row, column).getValue());
			Color stringColor = _model.getGrid().getGridParameters().getTokenTextColor();
			
			// If the opacity is being animated, change to the new colors.
			if (_tokenOpacityAnimation.containsKey(_model.getGrid().getToken(row, column))) {	
				final long currentTime = System.currentTimeMillis();
				final long animStartTime =
						_tokenOpacityAnimation.get(_model.getGrid().getToken(row, column)).getValue0();
				final long animEndTime =
						_tokenOpacityAnimation.get(_model.getGrid().getToken(row, column)).getValue1();
				final float animProgressionRatio = (currentTime - animStartTime) / (float)(animEndTime - animStartTime);
				
				stringColor = newComponentsAlteredColor(stringColor, -1, -1, -1,
						Math.max(0, Math.min(255, (int)(stringColor.getAlpha() * animProgressionRatio))));
				
				tokenColor = newComponentsAlteredColor(tokenColor, -1, -1, -1,
						Math.max(0, Math.min(255, (int)(tokenColor.getAlpha() * animProgressionRatio))));
			}
			
			// Draw the token
			g2.setColor(tokenColor);
			g2.fillRoundRect(offset.getValue0() + column*cellSideLength,
					offset.getValue1() + row*cellSideLength, cellSideLength, cellSideLength, cellSideLength/4, cellSideLength/4);
			
			// Draw the string (visually centered = not trivial)
			g2.setColor(stringColor);
			
			String str = _model.getGrid().getToken(row, column).toString();

			FontRenderContext renderContext = g2.getFontRenderContext();
			GlyphVector glyphVector = g2.getFont().createGlyphVector(renderContext, str);
			Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();
			Rectangle stringBounds = g2.getFontMetrics().getStringBounds(str, g2).getBounds();
			
			g2.drawString(str,
					offset.getValue0() + column*cellSideLength + cellSideLength / 2 - stringBounds.width / 2,
					offset.getValue1() + row*cellSideLength + visualBounds.height / 2 + cellSideLength / 2);
		}
		
		/**
		 * Allows to modify specified components of a given color.
		 *
		 * @param c the color that we wan't to modify
		 * @param r the new red component or -1 to keep c's one.
		 * @param g the new green component or -1 to keep c's one.
		 * @param b the new blue component or -1 to keep c's one.
		 * @param a the new alpha component or -1 to keep c's one.
		 * @return the color with modified components
		 */
		private Color newComponentsAlteredColor(Color c, int r, int g, int b, int a) {
			r = r == -1 ? c.getRed() : r; 
			g = g == -1 ? c.getGreen() : g;
			b = b == -1 ? c.getBlue() : b;
			a = a == -1 ? c.getAlpha() : a;
			return new Color(r, g, b, a);
		}
		
		/**
		 * @see javax.swing.JComponent#getPreferredSize()
		 */
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(getWidth(), getHeight());
		}

		/**
		 * Returns the side length of the tokens square.
		 *
		 * @return the the side length of the tokens square
		 */
		private int cellSideLength() {
			return Math.min(getWidth() / _model.getGrid().getGridParameters().getSize().getValue1(),
					getHeight() / _model.getGrid().getGridParameters().getSize().getValue0());
		}
		
		/**
		 * Gets the offset of the component on the pane.
		 *
		 * @return the offset
		 */
		private Pair<Integer, Integer> getOffset() {
		    final int gridNbRows = _model.getGrid().getGridParameters().getSize().getValue0();
            final int gridNbColumns = _model.getGrid().getGridParameters().getSize().getValue1();
			return new Pair<>(
					(getWidth() - cellSideLength() * gridNbColumns) / 2,
					(getHeight() - cellSideLength() * gridNbRows) / 2
				);
		}
	};
	
	/**
	 * Instantiates a new main view grid view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public MainViewGridView(final ImmutableModel model, final MainController controller) {
		super(model, controller);
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
	 */
	@Override
	Component getComponent() {
		return _jGrid;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
	    // If there is a message with the update event
		if (arg instanceof ModelUpdateEvent) {
			final long currentTime = System.currentTimeMillis();
			Map<String, Object> parameters = ((ModelUpdateEvent) arg).getParameters();
			
			// The event is a token move => add it to the animations map
			if (((ModelUpdateEvent) arg).getEvent().equals(ModelUpdateEvent.UpdateEvent.TOKEN_MOVE))
				_tokenInterpolations.put(
						new Pair<Integer, Integer>((Integer)parameters.get("row_number"),
								(Integer)parameters.get("column_number")),
						new Triplet<Grid.Direction, Long, Long>((Grid.Direction)parameters.get("token_direction"),
								currentTime,
								currentTime + _TOKEN_INTERPOLATION_DURATION));
			// The event is a opacity change => add it to the opacity animation map
			else if (((ModelUpdateEvent) arg).getEvent().equals(ModelUpdateEvent.UpdateEvent.TOKEN_VISIBILITY))
				_tokenOpacityAnimation.put(((ImmutableGridToken)parameters.get("token")),
						new Pair<Long, Long>(
								currentTime,
								currentTime + _TOKEN_OPACITY_ANIMATION_DURATION));
			// The event is a grid reset => reset every animation
			else if (((ModelUpdateEvent) arg).getEvent().equals(ModelUpdateEvent.UpdateEvent.GRID_RESET))
				resetAnimations();
			
			// Run a new frameGenerator if it's not already running.
			new Thread() {
				@Override
				public void run() {
					if (!_frameGeneratorRunning) {
						_frameGeneratorRunning = true;
						frameGenerator();
						_frameGeneratorRunning = false;
					}
				}
			}.start();
		}
	}

	/**
	 * Repaints the grid while there are animations running.
	 * MUST BE threadsafe.
	 */
	private void frameGenerator() {
		while (!_tokenInterpolations.isEmpty() || !_tokenOpacityAnimation.isEmpty()) {
			for (Entry<Pair<Integer, Integer>, Triplet<Direction, Long, Long>> tokenInterpolation : _tokenInterpolations.entrySet())
				if (tokenInterpolation.getValue().getValue2() <= System.currentTimeMillis())
					_tokenInterpolations.remove(tokenInterpolation.getKey());
			
			if (_tokenInterpolations.isEmpty())
				for (Entry<ImmutableGridToken, Pair<Long, Long>> tokenOpacityAnimation : _tokenOpacityAnimation.entrySet())
					if (tokenOpacityAnimation.getValue().getValue1() <= System.currentTimeMillis())
						_tokenOpacityAnimation.remove(tokenOpacityAnimation.getKey());
			
			_jGrid.paintImmediately(0, 0, _jGrid.getWidth(), _jGrid.getHeight());
			
			// Limit framerate
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Clears all the animations and sets a token opacity animation for each token.
	 */
	private void resetAnimations() {
	    // Clear all the animation
		_tokenInterpolations.clear();
		_tokenOpacityAnimation.clear();
		
		final long currentTime = System.currentTimeMillis();
		
		// Add a wonderful animation to make all the new token appear !
		Iterator<ImmutableGridToken> iterator = _model.getGrid().immutableIterator();
		while (iterator.hasNext()) {
			ImmutableGridToken token = iterator.next();
			_tokenOpacityAnimation.put(token,
					new Pair<Long, Long>(currentTime,
							currentTime + _TOKEN_OPACITY_ANIMATION_DURATION)
				);
		}
		
	}
}
