/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

/**
 * JComboItem is a data structure allowing to store a key-value entry for
 * SWING JComboBox.
 */
final class JComboItem
{
    
    /** The value. */
    public final String value;
    
    /** The label. */
    public final String label;
    
    /**
     * Contructor of JComboItem.
     *
     * @param val the val
     * @param lbl the lbl
     */
    JComboItem(String val, String lbl) {
        value = val; label = lbl;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return label;
    }
}
