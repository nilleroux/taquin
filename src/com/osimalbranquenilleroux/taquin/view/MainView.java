/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.osimalbranquenilleroux.taquin.controller.MainController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * Class MainView.
 * The main view of the game.
 */
public final class MainView extends AbstractView<MainController> {

	/** The game frame. */
	private final JFrame _mainFrame = new JFrame(_model.getStringProvider().getString("app_title"));
	
	/** The _jgrid. */
	private final MainViewGridView _jgrid = new MainViewGridView(_model, _controller);
	
	/**
	 * Instantiates a new main view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public MainView(final ImmutableModel model, final MainController controller) {
		super(model, controller);
		
		_mainFrame.setSize(800, 600);
		_mainFrame.setMinimumSize(new Dimension(300, 300));
		_mainFrame.setLocationByPlatform(true);
		_mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		_mainFrame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int res=JOptionPane.showConfirmDialog(_mainFrame, _model.getStringProvider().getString("are_you_sure"),
                        _model.getStringProvider().getString("quit"), JOptionPane.YES_NO_OPTION);
                if (res==0)
                    _controller.fireCloseEvent();
            }
        });
		
		JMenuBar menuBar = new JMenuBar();
			JMenu gameMenu = new JMenu(_model.getStringProvider().getString("game"));
			menuBar.add(gameMenu);
			JMenuItem newGameItem = new JMenuItem(_model.getStringProvider().getString("new_game"));
			newGameItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					_controller.fireNewGameEvent();
				}
			});
            JMenuItem highscoresItem = new JMenuItem(_model.getStringProvider().getString("display_highscores"));
            highscoresItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    _controller.fireShowHighscoresViewEvent();
                }
            });
			JMenuItem quitItem = new JMenuItem(_model.getStringProvider().getString("quit"));
			quitItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					_controller.fireCloseEvent();
				}
			});
			gameMenu.add(newGameItem);
			gameMenu.add(highscoresItem);
			gameMenu.add(quitItem);
		

		JMenu editMenu = new JMenu(_model.getStringProvider().getString("edit"));
		menuBar.add(editMenu);
			JMenuItem parametersItem = new JMenuItem(_model.getStringProvider().getString("parameters"));
			parametersItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					_controller.fireShowParametersViewEvent();
				}
			});
			editMenu.add(parametersItem);
			JMenuItem resetParametersItem = new JMenuItem(_model.getStringProvider().getString("reset_parameters"));
			resetParametersItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					_controller.fireReinitParametersEvent();
				}
			});
			editMenu.add(resetParametersItem);
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(menuBar, BorderLayout.NORTH);
		mainPanel.add(_jgrid.getComponent());
		
		_mainFrame.add(mainPanel);
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
	 */
	@Override
	Component getComponent() {
		return _mainFrame;
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
	 */
	@Override
	public void close() {
		super.close();
		_mainFrame.dispose();
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
		super.update(o, arg);
		_jgrid.update(o, arg);
	}

	/**
	 * Show "you have won the game" dialog.
	 */
	public void showWonView() {
		MainViewWonDialogView wonView = new MainViewWonDialogView(_model, _controller, _mainFrame);
		wonView.display();
	}

    public void showAskPlayerNameView() {
        MainViewAskPlayerNameView askPlayerNameView = new MainViewAskPlayerNameView(_model, _controller, _mainFrame);
        askPlayerNameView.display();
    }
}
