/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.osimalbranquenilleroux.taquin.controller.MainController;

/**
 * The Class ErrorDialogView. Shows a simple error Dialog and allows to set
 * the message in it.
 */
public final class ErrorDialogView extends AbstractView<MainController> {
	
	/** Parent frame (that should be frozen). */
	private final JFrame _parentFrame;
	
	/** The error message */
	private final String _errorMessage;
	
	/**
	 * Instantiates a new error dialog.
	 *
	 * @param parentView the parent view that contains the frame that should be frozen
	 * @param errorMessage the error message
	 */
	public ErrorDialogView(final AbstractView<?> parentView, final String errorMessage) {
	    // We prefer not using the model to make this class work in most cases
		super(null, null);
		if (parentView != null && parentView.getComponent() instanceof JFrame)
		    _parentFrame = (JFrame) parentView.getComponent();
		else
		    _parentFrame = null;
		_errorMessage = errorMessage;
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
	 */
	@Override
	Component getComponent() {
		return null;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#display()
	 */
	@Override
	public void display() {
	    JOptionPane.showMessageDialog(_parentFrame,
                _errorMessage,
                "Error !",
                JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
	 */
	@Override
	public void close() {
		//Nothing to do
	}
}
