/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.taquin.controller.HighscoresController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;
import com.osimalbranquenilleroux.taquin.model.Scores;

/**
 * Class HighscoresView.
 * Encompasses the parameters selection frame.
 */
public final class HighscoresView extends AbstractView<HighscoresController>
{
	
	/** The parameters frame. */
	private final JFrame _frame = new JFrame(_model.getStringProvider().getString("highscores"));
	
	/** The highscores (HTML) label. */
	private final JLabel _highscoresLabel = new JLabel();
	
	/** The number of rows of the grid spinner. */
	private final JSpinner _nbRowsSpinner;
	
	/** The number of cols of the grid spinner. */
	private final JSpinner _nbColsSpinner;
	
	/**
	 * Instantiates a new parameters view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public HighscoresView(final ImmutableModel model, final HighscoresController controller)
	{
		super(model, controller);
		
		// The frame
		_frame.setSize(400, 400);
		_frame.setMinimumSize(new Dimension(400, 400));
        _frame.setMaximumSize(new Dimension(400, 400));
		_frame.setLocationByPlatform(true);
		_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// The panels
		JPanel mainPanel = new JPanel(new BorderLayout());
		JPanel gridSizePanel = new JPanel(new FlowLayout());
		JPanel highscoresPanel = new JPanel(new FlowLayout());
		JPanel buttonPanel = new JPanel(new FlowLayout());
		
		_frame.add(mainPanel);
		mainPanel.add(gridSizePanel, BorderLayout.NORTH);
		mainPanel.add(highscoresPanel, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		
        JLabel nbRowsLabel = new JLabel(_model.getStringProvider().getString("number_of_rows"));
        SpinnerNumberModel nbRowsSpinnerModel =
                new SpinnerNumberModel(_model.getGrid().getGridParameters().getSize().getValue0().intValue(),
                        _model.getGrid().getGridParameters().getGridMinSize().getValue0().intValue(),
                        _model.getGrid().getGridParameters().getGridMaxSize().getValue0().intValue(),
                        1
                ); 
        _nbRowsSpinner = new JSpinner(nbRowsSpinnerModel);
        JLabel nbColsLabel = new JLabel(_model.getStringProvider().getString("number_of_columns"));
        SpinnerNumberModel nbColsSpinnerModel =
                new SpinnerNumberModel(_model.getGrid().getGridParameters().getSize().getValue1().intValue(), 
                        _model.getGrid().getGridParameters().getGridMinSize().getValue1().intValue(),
                        _model.getGrid().getGridParameters().getGridMaxSize().getValue1().intValue(),
                        1
                ); 
        _nbColsSpinner = new JSpinner(nbColsSpinnerModel);
        
        ChangeListener gridSizeChangeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                updateHighscoresLabel();
            }
        };
        
        _nbRowsSpinner.addChangeListener(gridSizeChangeListener);
        _nbColsSpinner.addChangeListener(gridSizeChangeListener);
        
        gridSizePanel.add(nbRowsLabel);
        gridSizePanel.add(_nbRowsSpinner);
        gridSizePanel.add(nbColsLabel);
        gridSizePanel.add(_nbColsSpinner);
        
        highscoresPanel.add(_highscoresLabel);
        
        JButton closeButton = new JButton(_model.getStringProvider().getString("close"));
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _controller.fireCloseEvent();
            }
        });
        JButton resetScores = new JButton(_model.getStringProvider().getString("reset_scores"));
        resetScores.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _controller.fireReinitScores();
            }
        });
        buttonPanel.add(closeButton);
        buttonPanel.add(resetScores);
        
        updateHighscoresLabel();
	}

    /**
     * Updates highscores label.
     */
    private void updateHighscoresLabel() {
        // Retrieve the highscores
        final String HTMLHighscores = Scores.getHTMLHighScores(
                new Pair<Integer, Integer>(
                        (Integer)_nbRowsSpinner.getValue(),
                        (Integer)_nbColsSpinner.getValue())
        );
        
        // If there is no highscore for this category, just say it.
        if ("".equals(HTMLHighscores)) {
            _highscoresLabel.setText(_model.getStringProvider().getString("nothing_to_display"));
            return;
        }
        
        _highscoresLabel.setText("<html>" + HTMLHighscores + "</html>");
    }
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _frame;
	 }
	 
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
 	 */
 	@Override
	 public void close() {
		 super.close();
		 _frame.dispose();
	 }
}
