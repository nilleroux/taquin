/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.HashSet;
import java.util.Locale;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.taquin.controller.ParametersController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;
import com.osimalbranquenilleroux.taquin.model.LocaleStringProvider;

/**
 * Class ParametersViewChangeGlobalParametersView.
 * View that allows to change global parameters.
 */
public class ParametersViewChangeGlobalParametersView extends AbstractView<ParametersController>
{
	
	/** The main panel. */
	private final JPanel _panel = new JPanel(new FlowLayout());
	private final JComboBox<JComboItem> _languagesComboBox;
	
	/**
	 * Instantiates a new parameters view change global parameters view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public ParametersViewChangeGlobalParametersView(final ImmutableModel model, final ParametersController controller)
	{
		super(model, controller);
		
		JPanel controls = new JPanel(new BorderLayout());
		JPanel lblsControls = new JPanel(new GridLayout(2,1));
		JPanel comboControls = new JPanel(new GridLayout(2,1));
		
		controls.add(lblsControls, BorderLayout.WEST);
		controls.add(comboControls, BorderLayout.EAST);
		
		final HashSet<Pair<String, String>> availableLanguages = LocaleStringProvider.getAvailableLanguages();
		
		JComboItem[] languagesItems = new JComboItem[availableLanguages.size()];
		JComboItem defaultItem = null;
		int i = 0;
		for (Pair<String, String> language : availableLanguages) {
		    languagesItems[i] = new JComboItem(language.getValue0(), language.getValue1());
		    
            final String[] localeCountryLanguage = language.getValue0().split("_");
            if (_model.getGlobalPreferences().getLocale().equals(
                    new Locale(localeCountryLanguage[0], localeCountryLanguage[1])))
                defaultItem = languagesItems[i];
            
            ++i;
		}
		
        _languagesComboBox = new JComboBox<>(languagesItems);
        _languagesComboBox.setSelectedItem(defaultItem);
        
        lblsControls.add(new JLabel(_model.getStringProvider().getString("language")));
        comboControls.add(_languagesComboBox);
		
		_panel.add(controls);
		_panel.add(new JLabel("<html><font color='red'>"
		        + _model.getStringProvider().getString("app_will_have_to_be_restarted")
                + "</font></html>"));
	}
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _panel;
	 }
 	
 	/**
 	 * Gets the selected locale
 	 */
 	Locale getLocale() {
        final String[] selectedLocale = ((JComboItem) _languagesComboBox.getSelectedItem()).value.split("_");
 	    return new Locale(selectedLocale[0], selectedLocale[1]);
 	}
}
