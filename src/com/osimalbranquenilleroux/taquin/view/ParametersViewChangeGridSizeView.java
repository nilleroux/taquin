/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.extra.FastMapInitializer;
import com.osimalbranquenilleroux.taquin.controller.ParametersController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * Class ParametersViewChangeGridSizeView.
 * View of the grid size parameter tab.
 */
public class ParametersViewChangeGridSizeView extends AbstractView<ParametersController>
{
	
	/** The main panel. */
	private final JPanel _panel = new JPanel(new FlowLayout());
	
	/** The number of rows spinner. */
	private final JSpinner _nbRowsSpinner;
	
	/** The number of columns spinner. */
	private final JSpinner _nbColumnsSpinner;
	
	/**
	 * Instantiates a new parameters view change grid size view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public ParametersViewChangeGridSizeView(final ImmutableModel model, final ParametersController controller)
	{
		super(model, controller);
		
		JPanel controls = new JPanel(new BorderLayout());
		JPanel lblsControls = new JPanel(new GridLayout(2,1));
		JPanel spinnersControls = new JPanel(new GridLayout(2,1));
		
		controls.add(lblsControls, BorderLayout.WEST);
		controls.add(spinnersControls, BorderLayout.EAST);
		
		SpinnerNumberModel nbRowsSpinnerModel =
				new SpinnerNumberModel(_model.getGrid().getGridParameters().getSize().getValue0().intValue(),
						_model.getGrid().getGridParameters().getGridMinSize().getValue0().intValue(),
						_model.getGrid().getGridParameters().getGridMaxSize().getValue0().intValue(),
						1
				); 
        _nbRowsSpinner = new JSpinner(nbRowsSpinnerModel);
        SpinnerNumberModel nbColsSpinnerModel =
				new SpinnerNumberModel(_model.getGrid().getGridParameters().getSize().getValue1().intValue(), 
						_model.getGrid().getGridParameters().getGridMinSize().getValue1().intValue(),
						_model.getGrid().getGridParameters().getGridMaxSize().getValue1().intValue(),
						1
				); 
        _nbColumnsSpinner = new JSpinner(nbColsSpinnerModel);
		
		lblsControls.add(new JLabel(_model.getStringProvider().getString("number_of_rows")));
		lblsControls.add(new JLabel(_model.getStringProvider().getString("number_of_columns")));
		spinnersControls.add(_nbRowsSpinner);
		spinnersControls.add(_nbColumnsSpinner);
		
		_panel.add(controls);
	}
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _panel;
	 }
	 
	 /**
 	 * Gets the spinners values.
 	 * "nb_rows" => number of rows
 	 * "nb_columns" => number of columns.
 	 *
 	 * @return the spinners values
 	 */
 	HashMap<String, Integer> getSpinnersValues() {
		 return FastMapInitializer.getHashMap(
				 new Pair<>("nb_rows", (Integer)_nbRowsSpinner.getValue()),
				 new Pair<>("nb_columns", (Integer)_nbColumnsSpinner.getValue()));
	 }
}
