/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.osimalbranquenilleroux.taquin.controller.MainController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * The Class MainViewAskPlayerNameView.
 * The view that asks for a name.
 */
final class MainViewAskPlayerNameView extends AbstractView<MainController> {
	
	/** Parent frame (that should be frozen). */
	private final JFrame _parentFrame;
	
	/**
	 * Instantiates a new main view won dialog view.
	 *
	 * @param model the model
	 * @param controller the controller
	 * @param parentFrame the parent frame
	 */
	public MainViewAskPlayerNameView(final ImmutableModel model, final MainController controller, final JFrame parentFrame) {
		super(model, controller);
		_parentFrame = parentFrame;
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
	 */
	@Override
	Component getComponent() {
		return null;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#display()
	 */
	@Override
	public void display() {
		String playerName = JOptionPane.showInputDialog(
		        _parentFrame,
		        _model.getStringProvider().getString("enter_your_name"),
		        _model.getStringProvider().getString("enter_your_name"),
		        JOptionPane.INFORMATION_MESSAGE);
		if (playerName != null) // If it was submited
		    _controller.fireNewPlayerName(playerName);
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
	 */
	@Override
	public void close() {
		//Nothing to do
	}
}
