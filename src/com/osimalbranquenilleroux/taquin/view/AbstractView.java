/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.Component;
import java.util.Observable;
import java.util.Observer;

import com.osimalbranquenilleroux.taquin.controller.AbstractController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * The Class AbstractView.
 * Every View must inherit this class. It defines the views basis.
 * This container class allows the Controller to handle a completely generic view, without paying attention
 * on the view engine. Thus, we can change the View without changing one line of code in the Controller.
 *
 * @param <GenericController> the controller type that the view depends on.
 */
public abstract class AbstractView <GenericController extends AbstractController> implements Observer {
	
	/** The Immutable model that the view should display. */
	protected final ImmutableModel _model;
	
	/** The controller that the views depends on. */
	protected final GenericController _controller;
	
	/**
	 * Basic constructor of a view.
	 *
	 * @param model the model
	 * @param controller the controller that the view depends on
	 */
	public AbstractView(final ImmutableModel model, final GenericController controller) {
		_model = model;
		_controller = controller;
	}
	
	/**
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 * Default behavior in case of model update : repaint the component of the view. 
	 */
	@Override
	public void update(Observable o, Object arg) {
		refresh();
	}

	/**
	 * Gets the component the view handles.
	 *
	 * @return the component or null if the view handles no component.
	 */
	abstract Component getComponent();
	
	/**
	 * Displays the view.
	 */
	public void display() {
		Component component = getComponent();
		component.setVisible(true);
		component.validate();
		component.repaint();
	}
	
	/**
	 * Hides the view.
	 */
	public void close() {
		getComponent().setVisible(false);
		refresh();
	}
	
	/**
	 * Refreshes the view.
	 */
	protected void refresh() {
		Component component = getComponent();
		component.validate();
		component.repaint();
	}
}
