/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.osimalbranquenilleroux.taquin.controller.ParametersController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * Class ParametersView.
 * Encompasses the parameters selection frame.
 */
public class ParametersView extends AbstractView<ParametersController>
{
	
	/** The parameters frame. */
	private final JFrame _frame = new JFrame(_model.getStringProvider().getString("parameters"));
	
	/**
	 * Instantiates a new parameters view.
	 *
	 * @param model the model
	 * @param controller the controller
	 */
	public ParametersView(final ImmutableModel model, final ParametersController controller)
	{
		super(model, controller);
		
		_frame.setSize(400, 400);
		_frame.setMinimumSize(new Dimension(300, 300));
		_frame.setLocationByPlatform(true);
		_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel mainPanel = new JPanel(new BorderLayout());
		JTabbedPane tabsPanel = new JTabbedPane();
		JPanel buttons = new JPanel(new FlowLayout());
		mainPanel.add(buttons, BorderLayout.SOUTH);
		
		final ParametersViewChangeGridSizeView changeGridSizeView =
				new ParametersViewChangeGridSizeView(_model, _controller);
		final ParametersViewChangeGridColorView changeGridColorView =
				new ParametersViewChangeGridColorView(_model, _controller, _frame);
		final ParametersViewChangeGlobalParametersView changeGlobalParametersView =
		        new ParametersViewChangeGlobalParametersView(_model, _controller);
		tabsPanel.addTab(_model.getStringProvider().getString("game"), changeGridSizeView.getComponent());
		tabsPanel.addTab(_model.getStringProvider().getString("coloring"), changeGridColorView.getComponent());
		tabsPanel.addTab(_model.getStringProvider().getString("global"), changeGlobalParametersView.getComponent());
        
        mainPanel.add(tabsPanel, BorderLayout.CENTER);
        
        JButton cancelButton = new JButton(_model.getStringProvider().getString("cancel"));
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_controller.fireCancelEvent();
			}
		});
		JButton okButton = new JButton(_model.getStringProvider().getString("ok"));
		
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
                
                final HashMap<String, Integer> spinnerValues = changeGridSizeView.getSpinnersValues();
                final HashMap<String, Color> colorValues = changeGridColorView.getColorsValues();
                final Locale selectedLocale = changeGlobalParametersView.getLocale();
		        
				_controller.fireNewParametersEvent(spinnerValues.get("nb_rows"),	spinnerValues.get("nb_columns"), 
						colorValues.get("token_start_color"), colorValues.get("token_end_color"),
						colorValues.get("token_text_color"), selectedLocale, true);
			}
		});
		JButton applyButton = new JButton(_model.getStringProvider().getString("apply"));
		applyButton.addActionListener(new ActionListener() {		    
			@Override
			public void actionPerformed(ActionEvent e) {
	            
	            final HashMap<String, Integer> spinnerValues = changeGridSizeView.getSpinnersValues();
	            final HashMap<String, Color> colorValues = changeGridColorView.getColorsValues();
	            final Locale selectedLocale = changeGlobalParametersView.getLocale();
	            
				_controller.fireNewParametersEvent(spinnerValues.get("nb_rows"), spinnerValues.get("nb_columns"), 
						colorValues.get("token_start_color"), colorValues.get("token_end_color"),
						colorValues.get("token_text_color"), selectedLocale, false);
			}
		});
		buttons.add(cancelButton);
		buttons.add(okButton);
		buttons.add(applyButton);
        
        _frame.add(mainPanel);
	}
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _frame;
	 }
	 
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#close()
 	 */
 	@Override
	 public void close() {
		 super.close();
		 _frame.dispose();
	 }
}
