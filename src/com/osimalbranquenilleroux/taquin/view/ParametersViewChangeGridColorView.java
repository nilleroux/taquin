/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.extra.FastMapInitializer;
import com.osimalbranquenilleroux.taquin.controller.ParametersController;
import com.osimalbranquenilleroux.taquin.model.ImmutableModel;

/**
 * Class ParametersViewChangeGridColorView.
 * View of the coloring parameter tab.
 */
public class ParametersViewChangeGridColorView extends AbstractView<ParametersController>
{
	
	/** The main panel. */
	private final JPanel _panel = new JPanel(new FlowLayout());
	
	/** The ColorPickerButtonView for token start color. */
	private final ColorPickerButtonView _gridTokenStartColorButton;
	
	/** The ColorPickerButtonView for token end color. */
	private final ColorPickerButtonView _gridTokenEndColorButton;
	
	/** The ColorPickerButtonView for token text color. */
	private final ColorPickerButtonView _gridTokenTextColorButton;
	
	/**
	 * Instantiates a new parameters view change grid color view.
	 *
	 * @param model the model
	 * @param controller the controller
	 * @param parentFrame the parent frame
	 */
	public ParametersViewChangeGridColorView(final ImmutableModel model, final ParametersController controller, final JFrame parentFrame)
	{
		super(model, controller);
		
		JPanel controlsWrapper = new JPanel(new BorderLayout());
		JPanel lblsControls = new JPanel(new GridLayout(3,1));
		JPanel controls = new JPanel(new GridLayout(3,1));
		
		controlsWrapper.add(lblsControls, BorderLayout.WEST);
		controlsWrapper.add(controls, BorderLayout.EAST);

		_gridTokenStartColorButton =
				new ColorPickerButtonView(_model, parentFrame,
						_model.getGrid().getGridParameters().getTokenStartColor());
		_gridTokenEndColorButton =
				new ColorPickerButtonView(_model, parentFrame,
						_model.getGrid().getGridParameters().getTokenEndColor());
		_gridTokenTextColorButton =
				new ColorPickerButtonView(_model, parentFrame,
						_model.getGrid().getGridParameters().getTokenTextColor());

		lblsControls.add(new JLabel(_model.getStringProvider().getString("token_start_color")));
		lblsControls.add(new JLabel(_model.getStringProvider().getString("token_end_color")));
		lblsControls.add(new JLabel(_model.getStringProvider().getString("text_color")));
		controls.add(_gridTokenStartColorButton.getComponent());
		controls.add(_gridTokenEndColorButton.getComponent());
		controls.add(_gridTokenTextColorButton.getComponent());
		
		_panel.add(controlsWrapper);
	}
	
	 /**
 	 * @see com.osimalbranquenilleroux.taquin.view.AbstractView#getComponent()
 	 */
 	@Override
	 Component getComponent() {
		 return _panel;
	 }
	 
	 /**
 	 * Returns a HashMap containing the colors.
 	 * "token_start_color" => start color
 	 * "token_end_color" => end color
 	 * "token_text_color" => text color
 	 *
 	 * @return the colors values
 	 */
 	HashMap<String, Color> getColorsValues() {
		 return FastMapInitializer.getHashMap(
				 new Pair<>("token_start_color", _gridTokenStartColorButton.getPickedColor()),
				 new Pair<>("token_end_color", _gridTokenEndColorButton.getPickedColor()),
				 new Pair<>("token_text_color", _gridTokenTextColorButton.getPickedColor()));
	 }
}
