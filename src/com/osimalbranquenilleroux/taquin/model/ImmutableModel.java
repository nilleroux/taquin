/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

/**
 * The Interface ImmutableModel.
 */
public interface ImmutableModel {
	
	/**
	 * Gets the immutable grid.
	 *
	 * @return the immutable grid
	 */
	public ImmutableGrid getGrid();
	
	/**
	 * Gets an immutable instance of the current global preferences.
	 * 
	 * @return the immutable global preferences
	 */
	public ImmutableGlobalPreferences getGlobalPreferences();
	
	/**
	 * Gets the string provider that should be use to display strings.
	 * 
	 * @return the string provider of this model
	 */
	public LocaleStringProvider getStringProvider();
	
	/**
	 * Gets an immutable instance of the current player.
	 * 
	 * @return the current player
	 */
	public ImmutablePlayer getPlayer();
}
