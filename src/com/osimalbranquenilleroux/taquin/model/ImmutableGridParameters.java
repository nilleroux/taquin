/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.awt.Color;

import org.javatuples.Pair;

/**
 * The Interface ImmutableGridParameters.
 */
public interface ImmutableGridParameters {
	
	/**
	 * Gets the minimal size of the Grid.
	 * The grid number of row should not be able to go under minimal number of rows.
	 * The grid number of columns should not be able to go under minimal number of columns.
	 *
	 * @return Pair<Integer:minimal number of rows, Integer:minimal number of columns>
	 */
	public Pair<Integer, Integer> getGridMinSize();
	
	/**
	 * Gets the maximal size of the Grid.
     * The grid number of row should not be able to go over maximal number of rows.
     * The grid number of columns should not be able to go over maximal number of columns.
     *
     * @return Pair<Integer:maximal number of rows, Integer:maximal number of columns>
	 */
	public Pair<Integer, Integer> getGridMaxSize();
	
	/**
	 * Gets the color of the first grid token.
	 *
	 * @return the color
	 */
	public Color getTokenStartColor();
	
	/**
	 * Gets the color of the last grid token.
	 *
	 * @return the color
	 */
	public Color getTokenEndColor();
	
	/**
	 * Gets the color of the text over tokens.
	 *
	 * @return the color
	 */
	public Color getTokenTextColor();
	
	/**
	 * Gets the current size of the grid.
	 *
	 * @return the size Pair<Integer:number of rows, Integer: number of columns>
	 */
	public Pair<Integer, Integer> getSize();
}
