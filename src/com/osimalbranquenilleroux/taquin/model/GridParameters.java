/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.awt.Color;
import java.util.Observable;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.extra.FastMapInitializer;

/**
 * Representation of the parameters of the Grid.
 */
public class GridParameters
	extends Observable
	implements ImmutableGridParameters,	PreferencesSavableLoadable {
	
	/** Minimal size of the Grid. Pair<Integer:number of rows, Integer: number of columns> */
	private static final Pair<Integer, Integer> _gridMinSize = new Pair<>(2,2);
	
	/** Maximal size of the Grid. Pair<Integer:number of rows, Integer: number of columns> */
	private static final Pair<Integer, Integer> _gridMaxSize = new Pair<>(10,10);
	
	/** The color of the first grid token. */
	private Color _tokenStartColor = new Color(255, 230, 0, 255);
	
	/** The color of the last grid token. (Intermediates tokens will be colored with gradient) */
	private Color _tokenEndColor = new Color(255, 230, 230, 0);
	
	/** The color of the text over tokens. */
	private Color _tokenTextColor = new Color(181, 181, 210, 255);
	
	/** Number of rows of the grid. */
	private int _nbRows = 3;
	
	/** Number of columns of the grid. */
	private int _nbColumns = 3;
	
	/**
	 * Instantiates a new grid parameters with a new size.
	 *
	 * @param nbRows the number of rows in the grid
	 * @param nbColumns the number of columns in the grid
	 */
	public GridParameters(final int nbRows, final int nbColumns) {
		if (nbRows < 0 || nbColumns < 0)
			throw new IllegalArgumentException("Number of rows or columns can't be negative.");
		
		resetDefaultParameters();
		
		Preferences prefs = Preferences.userNodeForPackage(this.getClass());
		prefs.putInt("gird_size_rows", nbRows);
		prefs.putInt("gird_size_columns", nbColumns);
		
		restorePreferencesHandleException();

		setChanged();
		notifyObservers(FastMapInitializer.getHashMap(
				new Pair<>("gird_size_rows", _nbRows),
				new Pair<>("gird_size_columns", _nbColumns)));
	}
	
	/**
	 * Instantiates a new grid parameters with default parameters (possibly restored from preferences).
	 */
	public GridParameters() {
		restorePreferencesHandleException();

		setChanged();
		notifyObservers(FastMapInitializer.getHashMap(
				new Pair<>("gird_size_rows", _nbRows),
				new Pair<>("gird_size_columns", _nbColumns)));
	}
	
	/**
	 * Resets the default parameters.
	 */
	private void resetDefaultParameters() {
		_tokenStartColor = new Color(255, 230, 0, 255);
		_tokenEndColor = new Color(255, 230, 230, 0);
		_tokenTextColor = new Color(181, 181, 210, 255);
		
		_nbRows = 3;
		_nbColumns = 3;
	}

	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getGridMinSize()
	 */
	public Pair<Integer, Integer> getGridMinSize() {
		return _gridMinSize;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getGridMaxSize()
	 */
	public Pair<Integer, Integer> getGridMaxSize() {
		return _gridMaxSize;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getTokenStartColor()
	 */
	public Color getTokenStartColor() {
		return _tokenStartColor;
	}
	
	/**
	 * Sets the color of the first grid token.
	 *
	 * @param tokenStartColor the color of the first grid token
	 */
	public void setTokenStartColor(Color tokenStartColor) {
		_tokenStartColor = tokenStartColor;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getTokenEndColor()
	 */
	public Color getTokenEndColor() {
		return _tokenEndColor;
	}
	
	/**
	 * Sets the color of the last grid token.
	 *
	 * @param tokenEndColor the color of the last grid token
	 */
	public void setTokenEndColor(Color tokenEndColor) {
		_tokenEndColor = tokenEndColor;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getTokenTextColor()
	 */
	public Color getTokenTextColor() {
		return _tokenTextColor;
	}
	
	/**
	 * Sets the color of the text over tokens.
	 *
	 * @param tokenTextColor the color of the text over tokens
	 */
	public void setTokenTextColor(Color tokenTextColor) {
		_tokenTextColor = tokenTextColor;
		setChanged();
		notifyObservers();
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridParameters#getSize()
	 */
	public Pair<Integer, Integer> getSize() {
		return new Pair<>(_nbRows, _nbColumns);
	}
	
	/**
	 * Sets the size of the grid.
	 *
	 * @param nbRows the number of rows
	 * @param nbColumns the number columns
	 */
	public void setSize(final int nbRows, final int nbColumns) {
		if (nbRows < 0 || nbColumns < 0)
			throw new IllegalArgumentException("Number of rows or columns can't be negative.");
		
		_nbRows = nbRows;
		_nbColumns = nbColumns;

		setChanged();
		notifyObservers(FastMapInitializer.getHashMap(
				new Pair<>("gird_size_rows", _nbRows),
				new Pair<>("gird_size_columns", _nbColumns)));
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#savePreferences()
	 */
	@Override
	public void savePreferences() {
		Preferences prefs = Preferences.userNodeForPackage(this.getClass());
		
		prefs.putInt("gird_size_rows", _nbRows);
		prefs.putInt("gird_size_columns", _nbColumns);
		prefs.putInt("token_start_color", _tokenStartColor.getRGB());
		prefs.putInt("token_end_color", _tokenEndColor.getRGB());
		prefs.putInt("token_text_color", _tokenTextColor.getRGB());
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#restorePreferences()
	 */
	@Override
	public void restorePreferences() throws BackingStoreException {
		Preferences prefs = Preferences.userNodeForPackage(this.getClass());
		
		_nbRows = prefs.getInt("gird_size_rows", _nbRows);
		_nbColumns = prefs.getInt("gird_size_columns", _nbColumns);
		_tokenStartColor = new Color(prefs.getInt("token_start_color", _tokenStartColor.getRGB()), true);
		_tokenEndColor = new Color(prefs.getInt("token_end_color", _tokenEndColor.getRGB()), true);
		_tokenTextColor = new Color(prefs.getInt("token_text_color", _tokenTextColor.getRGB()), true);
		
		prefs.flush();
	}
	
	/**
	 * Restore preferences, but sets up default preferences in case of exception.
	 */
	private void restorePreferencesHandleException() {
		try {
			restorePreferences();
		} catch (BackingStoreException exception) {
			System.err.println("BackingStoreException while restoring parameters : " + exception.getMessage());
			exception.printStackTrace();
			resetDefaultParameters();
		}
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#resetPreferences()
	 */
	@Override
	public void resetPreferences() throws BackingStoreException {
		Preferences prefs = Preferences.userNodeForPackage(this.getClass());
		prefs.removeNode();
		prefs.flush();
		resetDefaultParameters();

		setChanged();
		notifyObservers(FastMapInitializer.getHashMap(
				new Pair<>("gird_size_rows", _nbRows),
				new Pair<>("gird_size_columns", _nbColumns)));
	}
}
