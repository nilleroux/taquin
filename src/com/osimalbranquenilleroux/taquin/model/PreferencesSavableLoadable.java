/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.util.prefs.BackingStoreException;

/**
 * The Interface PreferencesSavableLoadable.
 * A PreferencesSavableLoadable class can save, reset and restore its preferences.
 */
public interface PreferencesSavableLoadable {
	
	/**
	 * Save preferences.
	 */
	public void savePreferences();
	
	/**
	 * Restore preferences.
	 *
	 * @throws BackingStoreException backing store exception
	 */
	public void restorePreferences() throws BackingStoreException;
	
	/**
	 * Reset preferences.
	 *
	 * @throws BackingStoreException the backing store exception
	 */
	public void resetPreferences() throws BackingStoreException;
}
