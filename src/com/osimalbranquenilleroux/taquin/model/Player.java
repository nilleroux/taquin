/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

/**
 * A player is a person who plays or has played the game.
 */
public class Player implements Comparable<Player>, ImmutablePlayer {
    
    /** Separator for players' string */
    private final static String PLAYER_STRING_SEPARATOR = ":";
    
    /** Defines the max length of the player's name */
    private final static int PLAYER_NAME_MAX_LENGTH = 10;
    
    /** Name of the player. */
    private String _name = null;
    
    /** The number of moves of the player. */
    private int _nbMoves;
    
    /**
     * Player default constructor.
     */
    public Player() { resetMoves(); }
    
    /**
     * Player constructor.
     */
    public Player(final String name, final int nbMoves) {
        _name = name;
        _nbMoves = nbMoves;
    }
    
    /**
     * Increases the number of moves of the player.
     */
    public void move() { ++_nbMoves; }
    
    /**
     * Reset moves.
     */
    public void resetMoves() { _nbMoves = 0; }
    
    /**
     * Sets the name of the player, removing non alphanumeric characters.
     *
     * @param name the new name
     */
    public void setName(String name) {
        if (name == null) {
            _name = null;
            return;
        }

        name = name.replaceAll("[\\" + PLAYER_STRING_SEPARATOR + "]", "");
        final int maxLength = name.length() < PLAYER_NAME_MAX_LENGTH ? name.length() : PLAYER_NAME_MAX_LENGTH;
        _name = name.substring(0, maxLength);
    }
    
    /**
     * @see com.osimalbranquenilleroux.taquin.model.ImmutablePlayer#getName()
     */
    public String getName() { return _name; }
    
    /* *
     * @see com.osimalbranquenilleroux.taquin.model.ImmutablePlayer#getMoves()
     */
    public int getMoves() { return _nbMoves; }
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return _name + PLAYER_STRING_SEPARATOR + _nbMoves;
    }
    
    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Player other_player) {
        if (_nbMoves < other_player._nbMoves) // A player is better if he has less moves
            return -1;
        if (_nbMoves == other_player._nbMoves)
            return 0;
        return 1;
    }
}
