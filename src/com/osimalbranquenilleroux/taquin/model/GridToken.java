/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.util.Iterator;
import java.util.Observable;

import org.javatuples.Pair;

/**
 * The Class GridToken.
 */
public class GridToken extends Observable implements ImmutableGridToken {
	
	/** True if the token should be hidden. */
	private boolean _hidden = false;
	
	/** The right position of the token in the grid. */
	private final int _value;
	
	/**
	 * Instantiates a new grid token with 0 as value.
	 */
	public GridToken() {
		this(0);
	}
	
	/**
	 * Instantiates a new grid token with a given value.
	 *
	 * @param value the value
	 */
	public GridToken(final int value) {
		_value = value;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridToken#getValue()
	 */
	public int getValue() {
		return _value;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridToken#getPosition(com.osimalbranquenilleroux.taquin.model.ImmutableGrid)
	 */
	public Pair<Integer, Integer> getPosition(final ImmutableGrid grid) {
		Iterator<ImmutableGridToken> iterator = grid.immutableIterator();
		int position = 0;
		while (iterator.hasNext()) {
			ImmutableGridToken token = iterator.next();
			
			if (token == this)
				return new Pair<>(
						position / grid.getGridParameters().getSize().getValue1(),
						position % grid.getGridParameters().getSize().getValue1());
			
			++position;
		}
		
		return null;
	}
	
	/**
	 * Switches the hidden status of the token.
	 */
	public void switchHidden() {
		_hidden ^= true;
		setChanged();
		ModelUpdateEvent event = new ModelUpdateEvent(ModelUpdateEvent.UpdateEvent.TOKEN_VISIBILITY);
		event.appendParameter("token", this);
		notifyObservers(event);
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridToken#isHidden()
	 */
	public boolean isHidden() {
		return _hidden;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGridToken#isWellPositioned(int)
	 */
	public boolean isWellPositioned(int currentPosition) {
		return _hidden || _value == currentPosition;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (_hidden)
			return new String();
		return String.valueOf(_value + 1);
	}
	
}
