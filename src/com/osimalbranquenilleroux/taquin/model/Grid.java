/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import org.javatuples.Pair;

import com.osimalbranquenilleroux.extra.FastMapInitializer;

/**
 * The grid.
 */
public class Grid extends Observable implements Iterable<GridToken>,Observer,ImmutableGrid {
	
    /** The random machine. */
    private static final Random _randomGenerator = new Random();
    
	/** Ordered tokens of the Grid. */
	private ArrayList<GridToken> _tokens;
	
	/** The parameters of the grid. */
	private final GridParameters _gridParameters;
	
	/**
	 * The Enum Direction.
	 */
	public static enum Direction {
	    
    	/** The north. */
        NORTH(0), 
        /** The east. */
        EAST(1), 
        /** The south. */
        SOUTH(2), 
        /** The west. */
        WEST(3);
	    
    	/** The code belonging to the direction. */
    	private final int _code;
	    
    	/** The number of directions. */
    	private static final int size = Direction.values().length;

	    /**
    	 * Instantiates a new direction.
    	 *
    	 * @param code the code of the direction.
    	 */
    	private Direction(int code) { _code = code; }
	    
	    /**
    	 * Returns a random direction.
    	 *
    	 * @return a random direction
    	 */
    	public static Direction randomDirection() {
	        return Direction.values()[_randomGenerator.nextInt(size)];
	    }
	    
	    /**
    	 * Returns a random direction, but allows to set directions that should not be returned.
    	 *
    	 * @param forbiddenDirections the forbidden directions
    	 * @return the random direction
    	 */
    	public static Direction randomDirection(final HashSet<Direction> forbiddenDirections) {
	    	HashSet<Direction> allowedDirections = new HashSet<Direction>(Arrays.asList(Direction.values()));
	    	allowedDirections.removeAll(forbiddenDirections);
	    	
	    	if (allowedDirections.isEmpty())
	    		return null;
	    	
	        int item = _randomGenerator.nextInt(allowedDirections.size());
	        int i = 0;
	        for (Direction dir : allowedDirections)
	        	if (i == item)
	        		return dir;
	        	else
	        		++i;
	        
	        return null;
	    }
	    
	    /**
    	 * Returns the difference of rows and columns if you walk <i>steps</i> times in current direction.
    	 *
    	 * @param steps the number of steps
    	 * @return a Pair<Integer:number of rows, Integer:number of columns>
    	 */
    	public Pair<Integer, Integer> delta(final int steps) {
	    	HashMap<Direction, Pair<Integer, Integer>> delta =
	    			FastMapInitializer.getHashMap(
    					new Pair<>(NORTH, new Pair<>(-steps, 0)),
    					new Pair<>(SOUTH, new Pair<>(steps, 0)),
    					new Pair<>(EAST, new Pair<>(0, steps)),
    					new Pair<>(WEST, new Pair<>(0, -steps))
	    			);
	    	
	    	return delta.get(this);
	    }
	    
	    /**
    	 * Opposite.
    	 *
    	 * @return the opposite to the current direction
    	 */
    	public Direction opposite() {
	    	return values()[(_code + 2) % size];
	    }
	}
	
	/**
	 * Instantiates a new ordered grid with standard parameters.
	 */
	public Grid() {
		this(new GridParameters());
	}
	
	/**
	 * Instantiates a new ordered grid with given parameters.
	 *
	 * @param parameters the parameters of the grid.
	 */
	public Grid(final GridParameters parameters) {
		_gridParameters = parameters;
		_gridParameters.addObserver(this);
		initTokens();
		
		for (GridToken token : _tokens)
			token.addObserver(this);
	}
	
	/**
	 * Inits the tokens.
	 */
	private void initTokens() {
		_tokens = new ArrayList<GridToken>();
		
		for (int i = 0, nbTokens = _gridParameters.getSize().getValue0() * _gridParameters.getSize().getValue1(); i < nbTokens; ++i)
			_tokens.add(new GridToken(i));
		
		_tokens.get(_tokens.size() - 1).switchHidden();
	}
	
	/**
	 * Returns the token at the given cell.
	 *
	 * @param row the row of the cell
	 * @param column the column of the cell
	 * @return the token
	 */
	public GridToken getToken(final int row, final int column) {
		if (row < 0 || column < 0)
			throw new IllegalArgumentException("Row or column can't be negative.");
		if (_gridParameters.getSize().getValue0() <= row || _gridParameters.getSize().getValue1() <= column)
			throw new IllegalArgumentException("Row or column exceeds grid boundaries.");
		
		return _tokens.get(_gridParameters.getSize().getValue1() * row + column);
	}
	
	/**
	 * Returns the <i>direction</i> neighbor token corresponding to the given cell.
	 *
	 * @param row the row
	 * @param column the column
	 * @param direction the direction
	 * @return the token
	 */
	public GridToken getToken(final int row, final int column, final Direction direction) {
		if (getForbiddenDirections(row, column).contains(direction))
			return null;
		
		return getToken(row + direction.delta(1).getValue0(), column + direction.delta(1).getValue1());
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#size()
	 */
	public int size() {
		return _tokens.size();
	}
	
	/**
	 * Moves a token in a given direction and notifies the observers.
	 *
	 * @param row the row
	 * @param column the column
	 * @param direction the direction
	 */
	public void moveToken(final int row, final int column, final Direction direction) {
		moveToken(row, column, direction, true);
	}
	
	/**
	 * Moves a token in a given direction.
	 *
	 * @param row the row
	 * @param column the column
	 * @param direction the direction
	 * @param notify We should notify observers of the change.
	 */
	public void moveToken(final int row, final int column, final Direction direction, boolean notify) {
		if (row < 0 || column < 0)
			throw new IllegalArgumentException("Row or column can't be negative.");
		if (_gridParameters.getSize().getValue0() <= row || _gridParameters.getSize().getValue1() <= column)
			throw new IllegalArgumentException("Row or column exceeds grid boundaries.");
		if (getForbiddenDirections(row, column).contains(direction))
			throw new IllegalArgumentException("Can't move outside the board.");
		Collections.swap(_tokens, getPosition(row, column),
				getPosition((row + direction.delta(1).getValue0()), (column + direction.delta(1).getValue1())));
		setChanged();
		if (notify) {
			ModelUpdateEvent event = new ModelUpdateEvent(ModelUpdateEvent.UpdateEvent.TOKEN_MOVE);
			event.appendParameter("token", _tokens.get(getPosition(row, column)));
			event.appendParameter("row_number", row + direction.delta(1).getValue0());
			event.appendParameter("column_number", column + direction.delta(1).getValue1());
			event.appendParameter("token_direction", direction);
			notifyObservers(event);
		}
	}
	
	/**
	 * Converts cell coordinates to a cell position in the token array.
	 *
	 * @param row the row of the cell
	 * @param column the column of the cell
	 * @return the position in the array
	 */
	private int getPosition(final int row, final int column) {
		return row * _gridParameters.getSize().getValue1() + column;
	}
	
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#getForbiddenDirections(int, int)
	 */
	public HashSet<Direction> getForbiddenDirections(final int row, final int column) {
		HashSet<Direction> forbiddenDirectionsSet = new HashSet<Direction>();
		if (row == 0)
			forbiddenDirectionsSet.add(Direction.NORTH);
		if (row == _gridParameters.getSize().getValue0() - 1)
			forbiddenDirectionsSet.add(Direction.SOUTH);
		if (column == 0)
			forbiddenDirectionsSet.add(Direction.WEST);
		if (column == _gridParameters.getSize().getValue1() - 1)
			forbiddenDirectionsSet.add(Direction.EAST);
		
		return forbiddenDirectionsSet;
	}
	
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#getAllowedDirections(int, int)
	 */
	public HashSet<Direction> getAllowedDirections(final int row, final int column) {
		HashSet<Direction> allowedDirections = new HashSet<>(Arrays.asList(Direction.values()));
		allowedDirections.removeAll(getForbiddenDirections(row, column));
		return allowedDirections;
	}
	
	/**
	 * Shuffles the Grid with size^2 iterations.
	 */
	void shuffle() {
		shuffle(_tokens.size()*_tokens.size());
	}
	
	/**
	 * Shuffles the grid with a given number of iterations (i.e. ~the number of random moves).
	 *
	 * @param iterations the number of iterations
	 */
	void shuffle(int iterations) {
		ArrayList<GridToken> hiddenTokens = getHiddenTokens();
		if (hiddenTokens.size() == 0 || _tokens.size() <= 1)
			return;

		do {
			for (int i = 0; i < iterations; ++i) {
				GridToken currentHiddenToken = hiddenTokens.get(i % hiddenTokens.size());
				int index = _tokens.indexOf(currentHiddenToken);
				int row = index / _gridParameters.getSize().getValue1();
				int column = index % _gridParameters.getSize().getValue1();
				
				moveToken(row, column, Direction.randomDirection(getForbiddenDirections(row, column)), false);
			}
			++iterations; // To prevent infinite loop, if grid is still well positioned
		} while (isWellPositioned());
		
		notifyObservers();
	}
	
	/**
	 * Returns an array containing the hidden tokens.
	 *
	 * @return the hidden tokens
	 */
	public ArrayList<GridToken> getHiddenTokens() {
		ArrayList<GridToken> hiddenTokens = new ArrayList<>();
		
		for (final GridToken token : _tokens)
			if (token.isHidden())
				hiddenTokens.add(token);
		
		return hiddenTokens;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#isWellPositioned()
	 */
	public boolean isWellPositioned() {
		int i = 0;
		for (GridToken token : _tokens)
			if (!token.isWellPositioned(i++))
				return false;
		return true;
	}

	/**
	 * @see java.lang.Iterable#iterator()
	 * Iterates over the grid.
	 */
	@Override
	public Iterator<GridToken> iterator() {
		return _tokens.iterator();
	}
	
	/**
	 * gridParameters getter.
	 *
	 * @return the current parameters of the grid
	 */
	public GridParameters getGridParameters() {
		return _gridParameters;
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#getColorOfPosition(int)
	 */
	public Color getColorOfPosition(int position) {
		final float ratio = ((float)position) / (size() - 1);
		final int r = Math.round(((float)_gridParameters.getTokenStartColor().getRed())*(1.f-ratio)
				+ ((float)_gridParameters.getTokenEndColor().getRed())*ratio);
		final int g = Math.round(((float)_gridParameters.getTokenStartColor().getGreen())*(1.f-ratio)
				+ ((float)_gridParameters.getTokenEndColor().getGreen())*ratio);
		final int b = Math.round(((float)_gridParameters.getTokenStartColor().getBlue())*(1.f-ratio)
				+ ((float)_gridParameters.getTokenEndColor().getBlue())*ratio);
		final int a = Math.round(((float)_gridParameters.getTokenStartColor().getAlpha())*(1.f-ratio)
				+ ((float)_gridParameters.getTokenEndColor().getAlpha())*ratio);
		return new Color(r, g, b, a);
	}

	/** 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 * Initializes the grid in case arg is a grid size change.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof HashMap<?, ?>) {
			@SuppressWarnings({ "rawtypes" })
			HashMap argument = (HashMap)arg;
			if (argument.containsKey("gird_size_rows") || argument.containsKey("gird_size_columns"))
				initTokens();
		}
		
		setChanged();
		notifyObservers(arg);
	}

	/* (non-Javadoc)
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableGrid#immutableIterator()
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator<ImmutableGridToken> immutableIterator() {
		return ((ArrayList)_tokens).iterator(); // This is safe (ref on GridToken implements ImmutableGridToken)
	}
}
