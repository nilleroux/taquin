/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import org.javatuples.Pair;

/**
 * The Interface ImmutableGridToken.
 */
public interface ImmutableGridToken {
	
	/**
	 * Gets the right position of the token in the grid.
	 *
	 * @return the value
	 */
	public int getValue();
	
	/**
	 * Gets the right coordinates of this token in a given grid.
	 *
	 * @param grid the grid
	 * @return the position Pair<Integer: right row, Integer: right column>
	 */
	public Pair<Integer, Integer> getPosition(final ImmutableGrid grid);
	
	/**
	 * Checks if the token is hidden.
	 *
	 * @return true, if it is hidden
	 */
	public boolean isHidden();
	
	/**
	 * Checks if the token is at its right position.
	 *
	 * @param currentPosition the current position of the token
	 * @return true, if it is well positioned
	 */
	public boolean isWellPositioned(int currentPosition);
}
