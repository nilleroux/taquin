/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.util.Locale;
import java.util.Observable;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Global preferences of the application.
 */
public class GlobalPreferences extends Observable implements
        PreferencesSavableLoadable, ImmutableGlobalPreferences
{
    
    /** The current locale. */
    private Locale _currentLocale = new Locale("fr", "FR");
    
    /**
     * Global Preferences default constructor.
     */
    GlobalPreferences() {
        restorePreferencesHandleException();

        setChanged();
        notifyObservers();
    }

    /**
     * Restore preferences and handle exceptions.
     */
    private void restorePreferencesHandleException()
    {
        try {
            restorePreferences();
        } catch (BackingStoreException exception) {
            System.err.println("BackingStoreException while restoring parameters : " + exception.getMessage());
            exception.printStackTrace();
            resetDefaultParameters();
        }
    }
    
    /**
     * Sets the locale.
     *
     * @param locale the new locale
     */
    public void setLocale(final Locale locale) {
        _currentLocale = locale;
        
        setChanged();
        notifyObservers();
    }
    
    /**
     * @see com.osimalbranquenilleroux.taquin.model.ImmutableGlobalPreferences#getLocale()
     */
    public Locale getLocale() {
        return _currentLocale;
    }
    
    /**
     * Resets the default parameters.
     */
    private void resetDefaultParameters() {
        _currentLocale = new Locale("fr", "FR");
    }

    /**
     * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#savePreferences()
     */
    @Override
    public void savePreferences()
    {
        Preferences prefs = Preferences.userNodeForPackage(this.getClass());
        
        prefs.put("locale_language", _currentLocale.getLanguage());
        prefs.put("locale_country", _currentLocale.getCountry());
    }

    /**
     * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#restorePreferences()
     */
    @Override
    public void restorePreferences() throws BackingStoreException
    {
        Preferences prefs = Preferences.userNodeForPackage(this.getClass());
        
        _currentLocale = new Locale(prefs.get("locale_language", _currentLocale.getLanguage()),
                prefs.get("locale_country", _currentLocale.getCountry()));
        
        prefs.flush();
    }

    /**
     * @see com.osimalbranquenilleroux.taquin.model.PreferencesSavableLoadable#resetPreferences()
     */
    @Override
    public void resetPreferences() throws BackingStoreException {
        Preferences prefs = Preferences.userNodeForPackage(this.getClass());
        prefs.removeNode();
        prefs.flush();
        resetDefaultParameters();
        
        setChanged();
        notifyObservers();
    }

}
