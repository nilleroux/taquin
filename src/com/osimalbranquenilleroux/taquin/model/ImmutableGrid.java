/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.awt.Color;
import java.util.HashSet;
import java.util.Iterator;

import com.osimalbranquenilleroux.taquin.model.Grid.Direction;

/**
 * The Interface ImmutableGrid.
 */
public interface ImmutableGrid {
	
	/**
     * returns An Immutable GridToken at the given cell coordinates.
     * 
     * @param row the row of the cell
     * @param column the column of the cell
     * @return the token
	 */
	public ImmutableGridToken getToken(final int row, final int column);
	
	/**
	 * Returns the <i>direction</i> neighbor token corresponding to the given cell.
     *
     * @param row the row
     * @param column the column
     * @param direction the direction
     * @return the token
	 */
	public ImmutableGridToken getToken(final int row, final int column, final Direction direction);
	
	/**
	 * Returns the number of tokens currently in the grid.
	 *
	 * @return the number of tokens currently in the grid
	 */
	public int size();
	
	/**
     * Returns the set of forbidden directions (i.e. directions that should not be accessed) from a given cell.
     *
     * @param row the row of the cell
     * @param column the column of the cell
     * @return the forbidden directions
     */
	public HashSet<Direction> getForbiddenDirections(final int row, final int column);
	
	/**
     * Returns the set of allowed directions (i.e. directions that could be accessed) from a given cell.
     *
     * @param row the row of the cell
     * @param column the column of the cell
     * @return the allowed directions
     */
	public HashSet<Direction> getAllowedDirections(final int row, final int column);
	
	/**
	 * Checks if the tokens are at their right position in the grid.
	 *
	 * @return true, if well positioned
	 */
	public boolean isWellPositioned();
	
	/**
     * @see java.lang.Iterable#iterator()
     * Iterates over the grid.
     */
	public Iterator<ImmutableGridToken> immutableIterator();
	
	/**
	 * Gets the immutable grid parameters.
	 *
	 * @return the immutable grid parameters
	 */
	public ImmutableGridParameters getGridParameters();
	
	/**
     * Returns the color of the token at the given position.
     *
     * @param position the position in the array.
     * @return the color
     */
	public Color getColorOfPosition(int position);
}
