/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;

import org.javatuples.Pair;

/**
 * This class aims to handle Scores
 */
public class Scores {
    
    /** Number of highscores returned per level. */
    private final static int HIGHSCORES_LINES = 10;
    
    /** The path to the scores files. */
    private final static String SCORES_PATH = "data/scores/";
    
    /** The model of the scores context */
    private final Model _model;
    
    /**
     * Scores context contructor
     * @param model The model
     */
    Scores(Model model) {
        _model = model;
    }
    
    /**
     * Gets the HTML String of the highscores corresponding to the given grid size.
     *
     * @param gridSize the grid size
     * @return the high scores
     */
    public static String getHTMLHighScores(final Pair<Integer, Integer> gridSize) {
        final ArrayList<Player> highScores = getSortedHighscores(gridSize);
        
        if (highScores.size() == 0)
            return "";
        
        String ret = "<table>";
        int i = 1;
        for (final Player player : highScores) {
            ret += "<tr>"
                +  "<td>" + i + ".</td>"
                +  "<td>" + escapeHTML(player.getName()) + "</td>"
                +  "<td>" + player.getMoves() + "</td>"
                +  "</tr>";
            if (HIGHSCORES_LINES < ++i)
                break;
        }
        
        ret += "</table>";
        
        return ret;
    }
    
    /**
     * Escape a string to prevent injecting HTML chars.
     *
     * @param s the non-HTML string
     * @return the escaped string
     */
    private static String escapeHTML(String s) {
        StringBuilder out = new StringBuilder(Math.max(16, s.length()));
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c > 127 || c == '"' || c == '<' || c == '>' || c == '&') {
                out.append("&#");
                out.append((int) c);
                out.append(';');
            } else {
                out.append(c);
            }
        }
        return out.toString();
    }
    
    /**
     * Adds a new score to the database.
     *
     * @param gridSize the grid size
     * @param player the player
     * @throws IOException 
     */
    public void addNewScore() throws IOException {
        Writer output;
        output = new BufferedWriter(new FileWriter(getScoresFilePath(_model.getGrid().getGridParameters().getSize()), true));
        output.append(_model.getPlayer().getName() + ":" + _model.getPlayer().getMoves() + "\n");
        output.close();
    }
    
    /**
     * Gets the sorted array of players, according to their scores.
     *
     * @param gridSize the grid size
     * @return the sorted highscores
     */
    private static ArrayList<Player> getSortedHighscores(final Pair<Integer, Integer> gridSize) {
        final ArrayList<Player> highScores = new ArrayList<>();
        
        FileInputStream fis;
        try {
            fis = new FileInputStream(getScoresFilePath(gridSize));
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            
            String line;
            while((line = reader.readLine()) != null){
                String[] playerStrings = line.split(":");
                if (playerStrings.length == 2)
                    highScores.add(new Player(playerStrings[0], Integer.valueOf(playerStrings[1])));
            }
            
            fis.close();
        } catch (IOException e) {}
        
        Collections.sort(highScores);
        
        return highScores;
    }
    
    /**
     * Gets the path to the scores file.
     *
     * @param gridSize the grid size
     * @return the scores file path
     */
    private static String getScoresFilePath(final Pair<Integer, Integer> gridSize) {
        return SCORES_PATH + gridSize.getValue0() + "x" + gridSize.getValue1() + ".scores";
    }
    
    /**
     * Delete scores of a given grid size.
     *
     * @param gridSize the grid size
     */
    public static void deleteScores(final Pair<Integer, Integer> gridSize) {
        final File file = new File(getScoresFilePath(gridSize));
        file.delete();
    }
    
    /**
     * Deletes every score file.
     */
    public static void resetScores() {
        for (File file : new File(SCORES_PATH).listFiles())
            file.delete();
    }
    
}
