/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.javatuples.Pair;

/**
 * Provides strings according to a given locale.
 * This class is immutable.
 */
public class LocaleStringProvider {
    /** Path to data.lang.properties files.
     * For example, French language France country file must be located there, with the name fr_FR.lang.properties */
    private static final String LOCALE_STRING_PROPERTIES_FILES_PATH = "data/lang/";
    /** Default locale when given locale could not be load */
    private static final Locale DEFAULT_LOCALE = new Locale("en", "US");
    /** Key => corresponding current locale string */
    private final Properties  _properties = new Properties();
    
    /**
     * LocaleStringProvider constructor.
     * @param locale The wanted locale
     */
    LocaleStringProvider(Locale locale) {
        try {
            loadPropertiesFromLocale(locale);
        }
        catch (FileNotFoundException e) {
            System.err.println("Unable to load any locale.");
            e.printStackTrace();
            System.exit(1);
        }
    }
    
    /**
     * Loads properties from a given locale.
     * If the given locale could not be loaded, then it loads the default locale.
     *
     * @param locale the locale
     * @throws FileNotFoundException When given locale lang file was not found, nor default locale file.
     */
    private void loadPropertiesFromLocale(Locale locale) throws FileNotFoundException {
        try {
            _properties.load(new BufferedReader(
                                new InputStreamReader(
                                    new FileInputStream(LOCALE_STRING_PROPERTIES_FILES_PATH + locale.getLanguage() + "_"
                                            + locale.getCountry() + ".lang.properties"),
                                    "UTF8")));
        } catch (FileNotFoundException e) {

            System.err.println("Could not load " + LOCALE_STRING_PROPERTIES_FILES_PATH
                    + locale.getLanguage() + "_" + locale.getCountry() + ".lang.properties");
            e.printStackTrace();
            
            if (!locale.equals(DEFAULT_LOCALE))
                loadPropertiesFromLocale(DEFAULT_LOCALE);
            else
                throw e;
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Gets the string corresponding to a given key.
     * 
     * @param key the key
     * @return the value or "???" if key was not found.
     */
    public String getString(String key) {
        final String ret = _properties.getProperty(key);
        if (ret == null)
            return "???";
        return ret;
    }
    
    /**
     * Gets the available locales
     * 
     * @return A set containing every available locales
     */
    public static HashSet<Locale> getAvailableLocales() {
        final HashSet<Locale> availableLocales = new HashSet<>();
        
        File folder = new File(LOCALE_STRING_PROPERTIES_FILES_PATH);
        File[] listOfFiles = folder.listFiles();
        Pattern langFilePattern = Pattern.compile("([a-z]+)_([A-Z]+)\\.lang\\.properties");

        for (int i = 0; i < listOfFiles.length; i++) {
            Matcher langFileMatcher = langFilePattern.matcher(listOfFiles[i].getName());
            if (langFileMatcher.matches()) {
                final Locale displayLocale = new Locale(langFileMatcher.group(1), langFileMatcher.group(2));
                if (displayLocale != null)
                    availableLocales.add(displayLocale);
            }
        }
        
        return availableLocales;
    }
    
    /**
     * Gets the available languages.
     * 
     * @return Pair<locale with "lang_country" format, locale display name>
     */
    public static HashSet<Pair<String, String>> getAvailableLanguages() {
        final HashSet<Pair<String, String>> availableLanguages = new HashSet<>();
        final HashSet<Locale> availableLocales = getAvailableLocales();
        
        for (Locale locale : availableLocales)
            availableLanguages.add(
                    new Pair<>(locale.getLanguage() + "_" + locale.getCountry(),
                            locale.getDisplayName(locale)));
        
        return availableLanguages;
    }
}
