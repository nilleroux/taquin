/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class ModelUpdateEvent is given as parameter to model's observers.
 * Please, respect parameters patterns corresponding to the event.
 */
public class ModelUpdateEvent {
	
	/**
	 * The update events available.
	 */
	public static enum UpdateEvent {
		
		/** No event.
		 * Empty parameters.
		 */
		NONE,
		
		/** The grid was reseted.
		 * Empty parameters.
		 */
		GRID_RESET,
		
		/** Token visibility has changed (hidden status).
		 * Parameters :
		 * "token" => GridToken the token whose visibility has changed
		 */
		TOKEN_VISIBILITY,
		
		/** One or more token has moved.
		 * Parameters :
		 * "token" => GridToken the token that was moved
		 * "row_nomber" => the final row of the token
		 * "column_number" => the final column of the token
		 * "token_direction" => the direction in which the token has moved
		 */
		TOKEN_MOVE,
	}
	
	/** The event. */
	private final UpdateEvent _event;
	
	/** The parameters. */
	private final Map<String, Object> _parameters = new HashMap<>();
	
	/**
	 * Instantiates a new model update event.
	 *
	 * @param event the event
	 */
	public ModelUpdateEvent(final UpdateEvent event) {
		_event = event;
	}
	
	/**
	 * Append parameter.
	 *
	 * @param parameterName the parameter name
	 * @param parameter the parameter
	 */
	public void appendParameter(final String parameterName, final Object parameter) {
		_parameters.put(parameterName, parameter);
	}
	
	/**
	 * Gets the parameters.
	 *
	 * @return the parameters
	 */
	public Map<String, Object> getParameters() {
		return Collections.unmodifiableMap(_parameters);
	}
	
	/**
	 * Gets the event.
	 *
	 * @return the event
	 */
	public UpdateEvent getEvent() {
		return _event;
	}
}
