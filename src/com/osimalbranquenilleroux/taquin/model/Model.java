/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

/**
 * The Class Model.
 */
public class Model extends Observable implements Observer, ImmutableModel { // Observable for the views,
															// Observer for sub-models.
    /** Global preferences */
    private final GlobalPreferences _globalPreferences = new GlobalPreferences();
	/** The grid. */
    private Grid _grid = null;
    /** The string provider */
    private LocaleStringProvider _localeStringProvider = new LocaleStringProvider(_globalPreferences.getLocale());
    /** The player. */
    private Player _player = new Player();
    /** The scores context. */
    private Scores _scoresContext = new Scores(this);
    
    /**
     * Model default constructor.
     */
    public Model() {
        Locale.setDefault(_globalPreferences.getLocale());
    }
	
	/**
	 * Gets the grid.
	 *
	 * @return reference to the grid
	 */
	public Grid getGrid() {
		return _grid;
	}
	
	/**
	 * Gets the global preferences
	 * 
	 * @return The global preferences
	 */
	public GlobalPreferences getGlobalPreferences() {
	    return _globalPreferences;
	}
	
	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	public Player getPlayer() {
	    return _player;
	}
	
	/**
	 * Gets the scores context
	 * 
	 * @return the scores context
	 */
	public Scores getScoresContext() {
	    return _scoresContext;
	}

	/**
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
		setChanged();
		notifyObservers(arg);
	}
	
	/**
	 * Inits a new shuffled grid with given parameters.
	 *
	 * @param gridParameters the parameters of the grid
	 */
	public void initGrid(final GridParameters gridParameters) {
		_grid = new Grid(gridParameters);
		_grid.addObserver(this);
		_grid.shuffle();
		setChanged();
		notifyObservers(new ModelUpdateEvent(ModelUpdateEvent.UpdateEvent.GRID_RESET));
	}
	
	/**
	 * Inits a new shuffled grid, keeping current grid parameters or instantiating new parameters if grid doesn't exist yet.
	 */
	public void initGrid() {
		if (_grid == null)
			initGrid(new GridParameters());
		else
			initGrid(_grid.getGridParameters());
	}
	
	/**
	 * @see com.osimalbranquenilleroux.taquin.model.ImmutableModel#getStringProvider()
	 */
	public LocaleStringProvider getStringProvider() {
	    return _localeStringProvider;
	}
	
}
