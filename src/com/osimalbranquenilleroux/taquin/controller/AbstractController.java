/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.osimalbranquenilleroux.taquin.model.Model;
import com.osimalbranquenilleroux.taquin.view.AbstractView;

/**
 * AbstractController is the Controller's Abstract type. Any Controller should inherit this class.
 */
public abstract class AbstractController {

	/** Map of views the controller handles. name_of_the_view => view */
	protected final Map<String, AbstractView<?>> _views = new HashMap<>();
	
	/** Model of this controller. */
	protected final Model _model;
	
	/** ControllerHandler of this Controller. */
	protected final ControllerHandler _handler;
	
	/**
	 * AbstractController constructor.
	 * This method is protected to allow children classes to override it. Moreover, a controller
	 * shouldn't be instantiated directly. The ControllerHandler aims to create it automatically.
	 *
	 * @param model Controller's model.
	 * @param handler Controller's handler.
	 */
	protected AbstractController(final Model model, final ControllerHandler handler) {
		_model = model;
		_handler = handler;
	}
	
	/**
	 * The entry point of the controller. Default behaviour : shows every view.
	 */
	public void act() {
		for (Entry<String, AbstractView<?>> viewEntry : _views.entrySet())
			viewEntry.getValue().display();
	}
	
	/**
	 * Adds the model observers.
	 */
	protected final void addModelObservers() {
		for (Map.Entry<String, AbstractView<?>> viewsEntries : _views.entrySet())
			_model.addObserver(viewsEntries.getValue());
	}
	
	/**
	 * Closes every view and asks the handler to close this controller.
	 */
	protected void close() {
		for (Entry<String, AbstractView<?>> viewEntry : _views.entrySet())
			viewEntry.getValue().close();
		_handler.destroyController(this);
	}
}
