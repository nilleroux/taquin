/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import com.osimalbranquenilleroux.taquin.model.Model;

/**
 * ControllerHandler is the only class that should instantiate/destroy Controllers.
 * It ensures that not two Controllers performing the same actions will be running at the same time.
 */
public class ControllerHandler {
	
	/** The controllers that are being handled by this controller. controller_name => controller */
	HashMap<String, AbstractController> _controllers = new HashMap<>();
	
	/**
	 * Launch controller.
	 * This method doesn't check Controller dynamic type.
	 *
	 * @param <GenericController> the type of controller to launch.
	 * @param model the model to give to the controller.
	 * @param name the name of the controller.
	 * @param type the class of the controller you want to instantiate.
	 * @return a reference to the new controller or the one has already been launched or null \
	 * if there is an error (e.g. invoke wrong Controller generic type).
	 */
	@SuppressWarnings("unchecked")
    public <GenericController extends AbstractController> GenericController
		launchController(Model model, String name, Class<GenericController> type) {
		try {
			if (_controllers.containsKey(name))
				return (GenericController)_controllers.get(name);
			GenericController ret = type.getDeclaredConstructor(Model.class, ControllerHandler.class)
					.newInstance(model, this);
			_controllers.put(name, ret);
			return ret;
		} catch(NoSuchMethodException e) {
			System.err.println("ControllerHandler could not find constructor : "
					+ e.toString() + "\nMessage : " + e.getMessage());
			return null;
		} catch(InvocationTargetException e) {
			System.err.println("ControllerHandler error while invoking constructor : "
					+ e.toString() + "\nMessage : " + e.getMessage());
			return null;
		} catch (ClassCastException e) {
		    System.err.println("ControllerHandler invoking wrong Controller generic type : "
                    + e.toString() + "\nMessage : " + e.getMessage());
            return null;
		} catch(Exception e) {
			System.err.println("ControllerHandler : Exception occurred : "
					+ e.toString() + "\nMessage : " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Destroys a controller.
	 *
	 * @param controller the controller to destroy.
	 */
	void destroyController(AbstractController controller) {
		_controllers.values().remove(controller);
	}
}
