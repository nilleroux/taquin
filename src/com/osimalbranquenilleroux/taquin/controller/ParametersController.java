/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.controller;

import java.awt.Color;
import java.util.Locale;

import com.osimalbranquenilleroux.taquin.model.GridParameters;
import com.osimalbranquenilleroux.taquin.model.LocaleStringProvider;
import com.osimalbranquenilleroux.taquin.model.Model;
import com.osimalbranquenilleroux.taquin.view.ParametersView;

/**
 * Handles the parameters.
 */
public final class ParametersController extends AbstractController {

	/**
	 * Instantiates a new parameters controller.
	 *
	 * @param model the model
	 * @param handler the handler
	 */
	protected ParametersController(final Model model, final ControllerHandler handler) {
		super(model, handler);
	}
	
	/**
	 * Updates parameters.
	 *
	 * @param nbRows the number of rows of the grid
	 * @param nbColumns the number of columns of the grid
	 * @param gridTokenStartColor the tokens' start color
	 * @param gridTokenEndColor the tokens' end color
	 * @param gridTokenTextColor the tokens' text color
	 * @param close the game should end
	 */
	public void fireNewParametersEvent(final int nbRows, final int nbColumns,
			final Color gridTokenStartColor, Color gridTokenEndColor, Color gridTokenTextColor,
			final Locale selectedLocale, boolean close) {
	    
		final GridParameters gridParameters = _model.getGrid().getGridParameters();
		
		// Check if the grid is higher than the minimum size. 
		if (nbRows < gridParameters.getGridMinSize().getValue0()
				|| nbColumns < gridParameters.getGridMinSize().getValue1())
			return;
		// Check if the selected language actually exists
		if (!LocaleStringProvider.getAvailableLocales().contains(selectedLocale))
		    return;
		
		// If the size has changed, update the parameters and create a new grid of the new size.
		if (nbRows != _model.getGrid().getGridParameters().getSize().getValue0()
				|| nbColumns != _model.getGrid().getGridParameters().getSize().getValue1()) {
			_model.getGrid().getGridParameters().setSize(nbRows, nbColumns);
			_model.initGrid(_model.getGrid().getGridParameters());
		}
		
		// Update colors
		gridParameters.setTokenStartColor(gridTokenStartColor);
		gridParameters.setTokenEndColor(gridTokenEndColor);
		gridParameters.setTokenTextColor(gridTokenTextColor);
		
		// Update default locale
        if (!(_model.getGlobalPreferences().getLocale().equals(selectedLocale)))
            _model.getGlobalPreferences().setLocale(selectedLocale);
        
        // Save the new prefs.
        _model.getGlobalPreferences().savePreferences();
		
		if (close) close();
	}
	
	/**
	 * {@inheritDoc}
	 * Launches a new ParametersView if it is not already launched.
	 */
	@Override
	public void act() {
		if (!_views.containsKey("parameters")) {
			_views.put("parameters", new ParametersView(_model, this));
		}
		super.act();
	}

	/**
	 * Closes the parameters without saving.
	 */
	public void fireCancelEvent() {
		_views.get("parameters").close();
		close();
	}
}
