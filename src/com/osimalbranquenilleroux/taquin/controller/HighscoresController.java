/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.controller;

import com.osimalbranquenilleroux.taquin.model.Model;
import com.osimalbranquenilleroux.taquin.model.Scores;
import com.osimalbranquenilleroux.taquin.view.HighscoresView;

/**
 * Handles the highscores (NOT the scores).
 */
public final class HighscoresController extends AbstractController {

	/**
	 * Instantiates a new highscores controller.
	 *
	 * @param model the model
	 * @param handler the handler
	 */
	protected HighscoresController(final Model model, final ControllerHandler handler) {
		super(model, handler);
	}
	
	/**
	 * {@inheritDoc}
	 * Launches a new HighscoresView if it is not already launched.
	 */
	@Override
	public void act() {
		if (!_views.containsKey("highscores")) {
			_views.put("highscores", new HighscoresView(_model, this));
		}
		super.act();
	}

	/**
	 * Closes the highscores.
	 */
	public void fireCloseEvent() {
		_views.get("highscores").close();
		close();
	}
	
	/**
	 * Resets the scores
	 */
	public void fireReinitScores() {
	    Scores.resetScores();
	}
}
