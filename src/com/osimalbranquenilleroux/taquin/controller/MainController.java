/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.prefs.BackingStoreException;

import com.osimalbranquenilleroux.taquin.model.Grid.Direction;
import com.osimalbranquenilleroux.taquin.model.GridToken;
import com.osimalbranquenilleroux.taquin.model.Model;
import com.osimalbranquenilleroux.taquin.view.ErrorDialogView;
import com.osimalbranquenilleroux.taquin.view.MainView;

/**
 * Main Controller. Handles main screen events.
 */
public final class MainController extends AbstractController {
	
	/**
	 * Instantiates a new main controller.
	 *
	 * @param model the model
	 * @param handler the handler
	 */
	protected MainController(final Model model, final ControllerHandler handler) {
		super(model, handler);
		
		_views.put("main_view", new MainView(_model, this));
		addModelObservers();

		initGame();
	}
	
	/**
	 * Inits the gmae.
	 */
	private void initGame() {
		_model.initGrid();
		_model.getPlayer().resetMoves();
	}

	/**
	 * Handles a cell click event.
	 *
	 * @param row the row clicked
	 * @param column the column clicked
	 */
	public void fireCellClickEvent(final int row, final int column) {
	    // Check if given position is correct.
		if (row < 0 || column < 0
				|| _model.getGrid().getGridParameters().getSize().getValue0() <= row
				|| _model.getGrid().getGridParameters().getSize().getValue1() <= column)
			return;
		
		// Move every token and update player's score.
		final HashSet<Direction> allowedDirections =  _model.getGrid().getAllowedDirections(row, column);
		for (Direction direction : allowedDirections) {
			if (_model.getGrid().getToken(row, column, direction).isHidden()) {
				_model.getGrid().moveToken(row, column, direction);
				_model.getPlayer().move();
				break;
			}
		}
		
		// If the player has won
		if (_model.getGrid().isWellPositioned()) {
		    // Reveal the tokens
			for (GridToken token : _model.getGrid().getHiddenTokens())
				token.switchHidden();
			
			// Ask the player for its name if we don't know it
			if (_model.getPlayer().getName() == null)
			    ((MainView)_views.get("main_view")).showAskPlayerNameView();
			
			// Then, if we know the player's name, try to add its score to the scoresContext.
			if (_model.getPlayer().getName() != null) {
                try {
                    _model.getScoresContext().addNewScore();
                } catch (IOException e) {
                    new ErrorDialogView(_views.get("main_view"), "Failed trying to save the score : " + e.getMessage())
                        .display();
			        e.printStackTrace();
		        }
			}
			
			((MainView)_views.get("main_view")).showWonView();
		}
	}
	
	/**
	 * Launches new game event.
	 */
	public void fireNewGameEvent() {
		initGame();
	}
	
	/**
	 * Instantiates a new ParametersController.
	 */
	public void fireShowParametersViewEvent() {
		_handler.launchController(_model, "parameters", ParametersController.class).act();
	}

	/**
	 * Saves the current preferences and terminates the app.
	 */
	public void fireCloseEvent() {
		_model.getGrid().getGridParameters().savePreferences();
		close();
	}

	/**
	 * Reinitializes the parameters.
	 */
	public void fireReinitParametersEvent() {
		try {
			_model.getGrid().getGridParameters().resetPreferences();
			_model.initGrid();
		} catch (BackingStoreException e) {
			new ErrorDialogView(_views.get("main_view"), "Failed trying to restore parameters : " + e.getMessage())
			    .display();
			e.printStackTrace();
		}
	}

    /**
     * Update player name event
     *
     * @param The new player name
     */
    public void fireNewPlayerName(final String playerName) {
        _model.getPlayer().setName(playerName);
    }

    /**
     * Instantiates a new HighscoreController and thus, displays the highscores.
     */
    public void fireShowHighscoresViewEvent() {
        _handler.launchController(_model, "highscores", HighscoresController.class).act();
    }
}
