/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

import com.osimalbranquenilleroux.taquin.model.Grid.Direction;

public class GridTester {
	@Test
	public void check2x2GridWasCorrectlyCreated() {
		Grid grid = new Grid(new GridParameters(2,2));
		assertEquals(0, grid.getToken(0,0).getValue());
		assertEquals(1, grid.getToken(0,1).getValue());
		assertEquals(2, grid.getToken(1,0).getValue());
		assertEquals(3, grid.getToken(1,1).getValue());
	}
	
	@Test
	public void check2x2ResizedGridWasCorrectlyCreated() {
		Grid grid = new Grid(new GridParameters(1, 1));
		grid.getGridParameters().setSize(2, 2);
		assertEquals(0, grid.getToken(0,0).getValue());
		assertEquals(1, grid.getToken(0,1).getValue());
		assertEquals(2, grid.getToken(1,0).getValue());
		assertEquals(3, grid.getToken(1,1).getValue());
		
		grid.getGridParameters().setSize(1, 1);
		assertEquals(0, grid.getToken(0,0).getValue());
	}
	
	@Test
	public void cantResizeToOrCreateNegativeSizedGrids() {
		final String errorString = "Number of rows or columns can't be negative.";
		Grid grid;
		boolean testFailed = false;
		
		try {
			grid = new Grid(new GridParameters(-2,2));
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid = new Grid(new GridParameters(2,-1));
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		grid = new Grid(new GridParameters(5,5));
		
		try {
			grid.getGridParameters().setSize(-2, 1);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid.getGridParameters().setSize(1, -2);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
	}
	
	@Test
	public void cantAccessOutOfBoundsToken() {
		final String errorString = "Row or column exceeds grid boundaries.";
		Grid grid = new Grid(new GridParameters(5,5));
		boolean testFailed = false;
		
		try {
			grid.getToken(15, 3);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid.getToken(3, 15);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid.getToken(5, 5);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
	}
	
	@Test
	public void cantAccessNegativePositionTokens() {
		final String errorString = "Row or column can't be negative.";
		Grid grid = new Grid(new GridParameters(5,5));
		boolean testFailed = false;
		
		try {
			grid.getToken(2, -3);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid.getToken(-2, 3);
		} catch (IllegalArgumentException e) {
			assertTrue(e.getMessage().equals(errorString));
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
	}
	
	@Test
	public void defaultGridTokensAreWellPositioned() {
		Grid grid = new Grid(new GridParameters(5,5));
		
		for (int i = 0; i < 5; ++i)
			for (int j = 0; j < 5; ++j)
				assertTrue(grid.getToken(i, j).isWellPositioned(i*5 + j));
	}
	
	@Test
	public void onlyLastGridTokenIsHidden() {
		Grid grid = new Grid(new GridParameters(4,5));
		
		for (int i = 0; i < 4; ++i)
			for (int j = 0; j < 4; ++j)
				assertFalse(grid.getToken(i, j).isHidden());
		
		for (int i = 0; i < 3; ++i)
			assertFalse(grid.getToken(i, 4).isHidden());
		
		assertTrue(grid.getToken(3, 4).isHidden());
	}
	
	@Test
	public void canMoveTokenInAGivenDirection() {
		Grid grid = new Grid(new GridParameters(2,2));
		
		assertEquals(0, grid.getToken(0, 0).getValue());
		assertEquals(1, grid.getToken(0, 1).getValue());
		
		grid.moveToken(0, 0, Grid.Direction.EAST);
		assertEquals(1, grid.getToken(0, 0).getValue());
		assertEquals(0, grid.getToken(0, 1).getValue());
		
		grid.moveToken(0, 1, Grid.Direction.SOUTH);
		assertEquals(3, grid.getToken(0, 1).getValue());
		assertEquals(0, grid.getToken(1, 1).getValue());
	}
	
	@Test
	public void cantMoveTokenOutsideTheBoard() {
		Grid grid = new Grid(new GridParameters(2,2));
		final String errorString = "Can't move outside the board.";
		boolean testFailed = true;
		
		try {
			grid.moveToken(1, 1, Grid.Direction.EAST);
		} catch (IllegalArgumentException e) {
			assertEquals(errorString, e.getMessage());
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
		
		try {
			grid.moveToken(1, 0, Grid.Direction.SOUTH);
		} catch (IllegalArgumentException e) {
			assertEquals(errorString, e.getMessage());
			testFailed = true;
		}
		assertTrue(testFailed);
		testFailed = false;
	}
	
	@Test
	public void canGetTokenOfAGivenDirection() {
		Grid grid = new Grid(new GridParameters(2,2));
		
		assertEquals(1, grid.getToken(0, 0, Grid.Direction.EAST).getValue());
		assertEquals(null, grid.getToken(0, 0, Grid.Direction.NORTH));
		assertEquals(0, grid.getToken(1, 0, Grid.Direction.NORTH).getValue());
	}
	
	@Test
	public void checkForbiddenDirections() {
		Grid grid = new Grid(new GridParameters(5,4));
		HashSet<Grid.Direction> forbiddenDirections;
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST, Grid.Direction.NORTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(0, 0)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST, Grid.Direction.NORTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(0, 3)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST, Grid.Direction.SOUTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(4, 3)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST, Grid.Direction.SOUTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(4, 0)));
		
		forbiddenDirections = new HashSet<Grid.Direction>();
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(2, 2)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(2, 3)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(2, 0)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.NORTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(0, 2)));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.SOUTH));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(4, 2)));
		
		grid = new Grid(new GridParameters(1,1));
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(
						Grid.Direction.NORTH,
						Grid.Direction.SOUTH,
						Grid.Direction.EAST,
						Grid.Direction.WEST));
		assertTrue(forbiddenDirections.equals(grid.getForbiddenDirections(0, 0)));
	}
	
	@Test
	public void checkAllowedDirections() {
		Grid grid = new Grid(new GridParameters(5,4));
		HashSet<Grid.Direction> allowedDirections;
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST, Grid.Direction.SOUTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(0, 0)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST, Grid.Direction.SOUTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(0, 3)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST, Grid.Direction.NORTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(4, 3)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST, Grid.Direction.NORTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(4, 0)));
		
		allowedDirections = new HashSet<Grid.Direction>(Arrays.asList(Direction.values()));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(2, 2)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.WEST,
						Grid.Direction.NORTH, Grid.Direction.SOUTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(2, 3)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST,
						Grid.Direction.NORTH, Grid.Direction.SOUTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(2, 0)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST,
						Grid.Direction.WEST, Grid.Direction.SOUTH));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(0, 2)));
		
		allowedDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.EAST,
						Grid.Direction.NORTH, Grid.Direction.WEST));
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(4, 2)));
		
		grid = new Grid(new GridParameters(1,1));
		allowedDirections =	new HashSet<Grid.Direction>();
		assertTrue(allowedDirections.equals(grid.getAllowedDirections(0, 0)));
	}
	
	@Test
	public void checkRandomDirection() {
		HashSet<Grid.Direction> forbiddenDirections;
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.SOUTH));
		for (int i = 0; i < 100; ++i)
			assertNotEquals(Grid.Direction.SOUTH, Grid.Direction.randomDirection(forbiddenDirections));
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(Grid.Direction.SOUTH, Grid.Direction.EAST));
		for (int i = 0; i < 100; ++i) {
			Grid.Direction direction = Grid.Direction.randomDirection(forbiddenDirections);
			assertNotEquals(Grid.Direction.SOUTH, direction);
			assertNotEquals(Grid.Direction.EAST, direction);
		}
		
		forbiddenDirections =
				new HashSet<Grid.Direction>(Arrays.asList(
						Grid.Direction.NORTH,
						Grid.Direction.EAST,
						Grid.Direction.SOUTH,
						Grid.Direction.WEST));
		for (int i = 0; i < 100; ++i)
			assertEquals(null, Grid.Direction.randomDirection(forbiddenDirections));
	}
	
	@Test
	public void checkDirectionDelta() {
		Grid.Direction direction = Grid.Direction.NORTH;
		assertEquals(Integer.valueOf(-4), direction.delta(4).getValue0());
		assertEquals(Integer.valueOf(0), direction.delta(4).getValue1());
		
		direction = Grid.Direction.EAST;
		assertEquals(Integer.valueOf(0), direction.delta(1).getValue0());
		assertEquals(Integer.valueOf(1), direction.delta(1).getValue1());
		
		direction = Grid.Direction.SOUTH;
		assertEquals(Integer.valueOf(2), direction.delta(2).getValue0());
		assertEquals(Integer.valueOf(0), direction.delta(2).getValue1());
		
		direction = Grid.Direction.WEST;
		assertEquals(Integer.valueOf(0), direction.delta(2).getValue0());
		assertEquals(Integer.valueOf(-2), direction.delta(2).getValue1());
	}
	
	@Test
	public void checkRandomShuffle() {
		Grid grid = new Grid(new GridParameters(5,4));
		grid.shuffle(1000);
		
		boolean[] present = new boolean[5*4];
		for (int i = 0; i < 5; ++i)
			for (int j = 0; j < 4; ++j)
				present[grid.getToken(i, j).getValue()] = true;

		for (int i = 0, end = 5*4; i < end; ++i)
			assertTrue(present[i]);
	}
	
	@Test
	public void defaultGridIsWellPositioned() {
		Grid grid = new Grid(new GridParameters(5,6));
		assertTrue(grid.isWellPositioned());
		
		grid = new Grid(new GridParameters(1,1));
		assertTrue(grid.isWellPositioned());
	}
	
	@Test
	public void oneIterationShuffledGridIsNecessarilyNotWellPositioned() {
		Grid grid = new Grid(new GridParameters(3,4));
		grid.shuffle(1);
		assertFalse(grid.isWellPositioned());
	}
}
