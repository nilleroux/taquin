/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.taquin.model;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Locale;

import org.javatuples.Pair;
import org.junit.Test;

public class LocaleStringProviderTester {
	@Test
	public void canLoadAvailableLocales() {
	    final HashSet<Locale> availableLocales = LocaleStringProvider.getAvailableLocales();
	    for (final Locale locale : availableLocales)
	        new LocaleStringProvider(locale);
	}
	
	@Test
	public void checkLanguagesKeysAreInTheRightFormat() {
	    final HashSet<Pair<String, String>> availableLanguages = LocaleStringProvider.getAvailableLanguages();
	    for (final Pair<String, String> language : availableLanguages)
	        assertTrue(language.getValue0().matches("[a-z]+_[A-Z]+"));
	}
	
	@Test
	public void checkThereIsAsMuchLanguagesAsLocales() {
	    assertTrue(LocaleStringProvider.getAvailableLanguages().size() == LocaleStringProvider.getAvailableLocales().size());
	}
	
	@Test
	public void canLoadUTF8String() {
	    final LocaleStringProvider stringProvider = new LocaleStringProvider(new Locale("fr", "FR"));
	    assertEquals("Paramètres", stringProvider.getString("parameters"));
	}
}
