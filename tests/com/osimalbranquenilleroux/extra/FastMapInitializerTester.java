/*******************************************************************************
 * This file is part of Taquin.
 *
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package com.osimalbranquenilleroux.extra;


import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.javatuples.Pair;
import org.junit.Test;

public class FastMapInitializerTester {
    
    @Test
    public void checkAllValuesArePresentInHashMap() {
        HashMap<Integer, Integer> myTestMap = 
                FastMapInitializer.getHashMap(new Pair<Integer, Integer>(1,2), new Pair<Integer, Integer>(3,4));
        assertEquals(new Integer(2), myTestMap.get(1));
        assertEquals(new Integer(4), myTestMap.get(3));
        assertEquals(null, myTestMap.get(2));
    }

}
